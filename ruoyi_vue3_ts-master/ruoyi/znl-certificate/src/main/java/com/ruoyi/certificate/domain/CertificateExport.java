package com.ruoyi.certificate.domain;

import java.util.List;

/**
 * @author love ice
 * @create 2023-12-08 20:50
 */
public class CertificateExport {
    private List<String> ids;

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }
}
