package com.ruoyi.certificate.service;

import com.ruoyi.certificate.domain.*;

import java.util.List;

/**
 * @author love ice
 * @create 2023-12-20 17:44
 */
public interface IZnlCertificateInfoEchartsService {
     GameTypeYearVo gameTypeYear();

     CertificateIncreaseVo certificateIncrease(CertificateIncreaseReq request);

     List<CertificateRankVo> certificateRank(CertificateIncreaseReq request);
}
