package com.ruoyi.web.controller.certificate;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import com.ruoyi.certificate.domain.CertificateExport;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.certificate.domain.ZnlCertificateInfo;
import com.ruoyi.certificate.service.IZnlCertificateInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 证书信息管理Controller
 * 
 * @author zhangch
 * @date 2023-12-07 23:37:50
 */
@ApiSort(value = 116)
@Api(tags = "证书信息管理控制器")
@RestController
@RequestMapping("/commendation/info")
public class ZnlCertificateInfoController extends BaseController {

    @Resource
    IZnlCertificateInfoService znlCertificateInfoService;

    /**
     * 查询证书信息管理列表
     * @param znlCertificateInfo 证书信息管理对象
     * @return 证书信息管理分页数据
     */
    @ApiOperationSupport(order = 1)
    @ApiOperation(value = "查询证书信息管理列表")
    @ApiImplicitParam(name = "znlCertificateInfo", value = "证书信息管理对象", paramType = "query", dataTypeClass = ZnlCertificateInfo.class)
    @PreAuthorize("@ss.hasPermi('commendation:info:list')")
    @GetMapping(value = "/list")
    public TableDataInfo<List<ZnlCertificateInfo>> list(ZnlCertificateInfo znlCertificateInfo) {
        startPage();
        return getDataTable(this.znlCertificateInfoService.selectZnlCertificateInfoList(znlCertificateInfo));
    }

    /**
     * 导出证书信息管理列表
     *
     * @param znlCertificateInfo 证书信息管理对象
     * @return 证书信息管理列表
     */
    @ApiOperationSupport(order = 2)
    @ApiOperation(value = "导出证书信息管理列表")
    @ApiImplicitParam(name = "znlCertificateInfo", value = "证书信息管理对象", paramType = "query", dataTypeClass = ZnlCertificateInfo.class)
    @PreAuthorize("@ss.hasPermi('commendation:info:export')")
    @Log(title = "证书信息管理", businessType = BusinessType.EXPORT)
    @GetMapping(value = "/export")
    public AjaxResult<String> export(ZnlCertificateInfo znlCertificateInfo) {
        List<ZnlCertificateInfo> list = this.znlCertificateInfoService.selectZnlCertificateInfoList(znlCertificateInfo);
        ExcelUtil<ZnlCertificateInfo> util = new ExcelUtil<>(ZnlCertificateInfo.class);
        return util.exportExcel(list, "info");
    }

    /**
     * 导出证书信息管理列表(返回流)
     *
     * @param ids 证书信息管理对象
     * @param response  返回
     *
     */
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "导出证书信息管理列表(返回流)")
    @ApiImplicitParam(name = "znlCertificateInfo", value = "证书信息管理对象", paramType = "query", dataTypeClass = ZnlCertificateInfo.class)
    @PreAuthorize("@ss.hasPermi('commendation:info:export')")
    @Log(title = "证书信息管理", businessType = BusinessType.EXPORT)
    @PostMapping(value = "/exportIds", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void exportByStreamExportIds(CertificateExport ids,HttpServletResponse response) {
        Long[] longArray = ids.getIds().stream().map(Long::valueOf).toArray(Long[]::new);
        new ExcelUtil<>(ZnlCertificateInfo.class).exportExcel(response, this.znlCertificateInfoService.selectZnlCertificateInfoByIds(longArray), "证书信息管理");
    }

    /**
    * 导出证书信息管理列表(返回流)
    *
    * @param znlCertificateInfo 证书信息管理对象
    * @param response  返回
    *
    */
    @ApiOperationSupport(order = 3)
    @ApiOperation(value = "导出证书信息管理列表(返回流)")
    @ApiImplicitParam(name = "znlCertificateInfo", value = "证书信息管理对象", paramType = "query", dataTypeClass = ZnlCertificateInfo.class)
    @PreAuthorize("@ss.hasPermi('commendation:info:export')")
    @Log(title = "证书信息管理", businessType = BusinessType.EXPORT)
    @PostMapping(value = "/exportByStream", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void exportByStream(ZnlCertificateInfo znlCertificateInfo, HttpServletResponse response) {
        new ExcelUtil<>(ZnlCertificateInfo.class).exportExcel(response, this.znlCertificateInfoService.selectZnlCertificateInfoList(znlCertificateInfo), "证书信息管理");
    }


    @Log(title = "证书信息导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('commendation:info:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception
    {
        ExcelUtil<ZnlCertificateInfo> util = new ExcelUtil<ZnlCertificateInfo>(ZnlCertificateInfo.class);
        List<ZnlCertificateInfo> certificateList = util.importExcel(file.getInputStream());
        String message;
        try {
            message  = znlCertificateInfoService.importCertificateData(certificateList);
        }catch (Exception e){
            return AjaxResult.error(e.getMessage());
        }
        return AjaxResult.success(message);
    }

    /**
     * 导入证书模板
     *
     * @return
     */
    @PostMapping(value = "/importTemplate")
    public void importTemplate(HttpServletResponse response) {
        new ExcelUtil<>(ZnlCertificateInfo.class).exportExcel(response, new ArrayList<ZnlCertificateInfo>(), "证书信息管理");

//        ExcelUtil<ZnlCertificateInfo> util = new ExcelUtil<>(ZnlCertificateInfo.class);
//        return util.importTemplateExcel("证书数据");
    }




    /**
     * 获取证书信息管理详细信息
     *
     * @param id 证书信息管理主键
     */
    @ApiOperationSupport(order = 4)
    @ApiOperation(value = "获取证书信息管理详细信息")
    @ApiImplicitParam(name = "id", value = "证书信息管理主键", paramType = "path", dataTypeClass = Long.class, required = true)
    @PreAuthorize("@ss.hasPermi('commendation:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult<ZnlCertificateInfo> getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(this.znlCertificateInfoService.selectZnlCertificateInfoById(id));
    }

    /**
     * 新增证书信息管理
     *
     *  @param znlCertificateInfo 证书信息管理对象
     */
    @ApiOperationSupport(order = 5)
    @ApiOperation(value = "新增证书信息管理")
    @PreAuthorize("@ss.hasPermi('commendation:info:add')")
    @Log(title = "证书信息管理", businessType = BusinessType.INSERT)
    @PostMapping(value = "/add")
    public AjaxResult<String> add(@RequestBody ZnlCertificateInfo znlCertificateInfo) {
        return toAjax(this.znlCertificateInfoService.insertZnlCertificateInfo(znlCertificateInfo));
    }

    /**
     * 修改证书信息管理
     *
     * @param  znlCertificateInfo 证书信息管理对象
     */
    @ApiOperationSupport(order = 6)
    @ApiOperation(value = "修改证书信息管理")
    @PreAuthorize("@ss.hasPermi('commendation:info:edit')")
    @Log(title = "证书信息管理", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/edit")
    public AjaxResult<String> edit(@RequestBody ZnlCertificateInfo znlCertificateInfo) {
        return toAjax(this.znlCertificateInfoService.updateZnlCertificateInfo(znlCertificateInfo));
    }

    /**
     * 删除证书信息管理
     *
     * @param ids
     */
    @ApiOperationSupport(order = 7)
    @ApiOperation(value = "删除证书信息管理")
    @ApiImplicitParam(name = "ids", value = "证书信息管理主键数组", paramType = "path", dataTypeClass = Long.class, allowMultiple = true, required = true)
    @PreAuthorize("@ss.hasPermi('commendation:info:remove')")
    @Log(title = "证书信息管理", businessType = BusinessType.DELETE)
    @DeleteMapping(value = "/{ids}")
    public AjaxResult<String> remove(@PathVariable Long[] ids) {
        return toAjax(this.znlCertificateInfoService.deleteZnlCertificateInfoByIds(ids));
    }

}
