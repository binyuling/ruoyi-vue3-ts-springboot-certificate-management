package com.ruoyi.web.controller.certificate;

import com.ruoyi.certificate.domain.*;
import com.ruoyi.certificate.service.IZnlCertificateInfoEchartsService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 证书首页的报表接口
 * @author love ice
 * @create 2023-12-20 17:40
 */

@RestController
@RequestMapping("/commendation/echarts")
public class ZnlCertificateInfoEchartsController extends BaseController {

    /**
     * 年度证书赛事分类统计图
     */
    @Autowired
    IZnlCertificateInfoEchartsService znlCertificateInfoEchartsService;
    @GetMapping("/game-type-year")
    public GameTypeYearVo gameTypeYear(){
        return znlCertificateInfoEchartsService.gameTypeYear();
    }

    /**
     * 证书赛事数量统计图(按获奖时间统计)
     * @param request
     * @return
     */
    @GetMapping("/certificate-increase")
    public CertificateIncreaseVo certificateIncrease(CertificateIncreaseReq request){
        CertificateIncreaseVo certificateIncreaseVo = znlCertificateInfoEchartsService.certificateIncrease(request);
        return certificateIncreaseVo;
    }

    /**
     * 证书赛事数量统计图(按获奖时间统计)
     * @param request
     * @return
     */
    @GetMapping("/certificate-year-time")
    public List<CertificateRankVo> certificateYearTime(CertificateIncreaseReq request){
        return znlCertificateInfoEchartsService.certificateRank(request);
    }



}
