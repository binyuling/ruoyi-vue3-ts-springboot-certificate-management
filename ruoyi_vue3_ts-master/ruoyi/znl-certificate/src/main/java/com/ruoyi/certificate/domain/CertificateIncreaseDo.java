package com.ruoyi.certificate.domain;

import java.util.Date;

/**
 * @author love ice
 * @create 2023-12-22 16:49
 */
public class CertificateIncreaseDo {
    private String acquisitionDate;
    private String certificateType;
    private Integer count;

    public String getAcquisitionDate() {
        return acquisitionDate;
    }

    public void setAcquisitionDate(String acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
