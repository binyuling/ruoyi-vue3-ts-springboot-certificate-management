import request from "@/utils/request";

/**
 * 查询所有证书分布
 * echarts
 * @param {any} query
 * @returns 证书信息
 */
export const gameTypeYear = async () => {
	return await request({
		url: "/commendation/echarts/game-type-year",
		method: "get",
	});
};


/**
 * 证书数量统计图(按照获奖时间统计)
 *
 * @param {object} query
 */
export const certificateIncrease = async (query: object) => {
   
    return await request({
      url: '/commendation/echarts/certificate-increase',
      method: 'get',
      params: query
    });
  };



  /**
 * 证书获奖级别统计图(按照年度统计)
 *
 * @param {object} query
 */
export const certificateRank = async (query: object) => {
    
    return await request({
      url: '/commendation/echarts/certificate-year-time',
      method: 'get',
      params: query
    });
  };