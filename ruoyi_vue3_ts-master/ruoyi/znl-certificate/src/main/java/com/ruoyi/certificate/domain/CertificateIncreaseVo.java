package com.ruoyi.certificate.domain;

import java.io.Serializable;
import java.util.List;

/**
 * option = {
 *         tooltip: {
 *             trigger: 'axis'
 *         },
 *         legend: {
 *             data: ['国家级', '省部级', '地市级', '校级']
 *         },
 *         xAxis: {
 *             type: 'category',
 *             boundaryGap: false,
 *             data: ['2023-12-21', '2023-12-22', '2023-12-23', '2023-12-24', '2023-12-25', '2023-12-26', '2023-12-27']
 *         },
 *         yAxis: {
 *             type: 'value'
 *         },
 *         series: [
 *             {
 *                 name: '国家级',
 *                 type: 'line',
 *
 *                 data: [50, 12, 21, 54, 260, 830, 710]
 *             },
 *             {
 *                 name: '省部级',
 *                 type: 'line',
 *
 *                 data: [30, 182, 434, 791, 390, 30, 10]
 *             },
 *             {
 *                 name: '地市级',
 *                 type: 'line',
 *
 *                 data: [0, 1132, 601, 234, 120, 90, 20]
 *             },
 *             {
 *                 name: '校级',
 *                 type: 'line',
 *
 *                 data: [0, 876, 601, 234, 12, 90, 20]
 *             }
 *         ]
 *     };
 * @author love ice
 * @create 2023-12-22 16:24
 */
public class CertificateIncreaseVo implements Serializable {
    private XAxis xAxis;

    private List<Series> series;

    public XAxis getxAxis() {
        return xAxis;
    }

    public void setxAxis(XAxis xAxis) {
        this.xAxis = xAxis;
    }

    public List<Series> getSeries() {
        return series;
    }

    public void setSeries(List<Series> series) {
        this.series = series;
    }

   public class XAxis {
        private String type;
        private Boolean boundaryGap;
        private List<String> data;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Boolean getBoundaryGap() {
            return boundaryGap;
        }

        public void setBoundaryGap(Boolean boundaryGap) {
            this.boundaryGap = boundaryGap;
        }

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }
    }

    public class Series {
        private String name;
        private String type;
        private List<String> data;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<String> getData() {
            return data;
        }

        public void setData(List<String> data) {
            this.data = data;
        }
    }
}
