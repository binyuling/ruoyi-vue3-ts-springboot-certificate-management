package com.ruoyi.certificate.domain;

/**
 * @author love ice
 * @create 2023-12-25 17:30
 */
public enum CertificateRankEnum {
    SPECIAL_PRIZE(0, "特等奖"),
    GOLD_PRIZE(11, "金奖"),
    FIRST_PRIZE(1, "一等奖"),
    SILVER_PRIZE(12, "银奖"),
    SECOND_PRIZE(2, "二等奖"),
    BRONZE_PRIZE(13, "铜奖"),
    THIRD_PRIZE(3, "三等奖"),
    EXCELLENT_PRIZE(4, "优秀奖"),
    FINALIST_PRIZE(5, "入围奖"),
    EXCELLENT_TEACHER(6, "优秀指导老师");

    private final int code;
    private final String description;

    CertificateRankEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static String getDescriptionByCode(int code) {
        for (CertificateRankEnum award : CertificateRankEnum.values()) {
            if (award.code == code) {
                return award.description;
            }
        }
        return "Unknown";
    }
}
