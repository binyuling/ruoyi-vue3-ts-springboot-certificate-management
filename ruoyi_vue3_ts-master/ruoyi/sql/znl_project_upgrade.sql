/*
 Navicat Premium Data Transfer

 Source Server         : 本地的mysql
 Source Server Type    : MySQL
 Source Server Version : 50728
 Source Host           : localhost:3306
 Source Schema         : znl_project_upgrade

 Target Server Type    : MySQL
 Target Server Version : 50728
 File Encoding         : 65001

 Date: 10/03/2024 17:28:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  `swagger` int(11) NULL DEFAULT 0 COMMENT '是否生成swagger注释(0:否，1：是)',
  `excel_export` int(11) NULL DEFAULT 0 COMMENT '是否生成导出excel注释(0:否，1：是)',
  `vue_version` int(11) NULL DEFAULT 2 COMMENT 'vue版本(2或者3，默认是2)',
  PRIMARY KEY (`table_id`) USING BTREE,
  UNIQUE INDEX `table_name_unique_index`(`table_name`) USING BTREE COMMENT '表名唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 'znl_certificate_info', '证书信息表', NULL, NULL, 'ZnlCertificateInfo', 'crud', 'com.ruoyi.certificate', 'commendation', 'info', '证书信息管理', 'zhangch', '0', '/', '{\"parentMenuId\":\"2000\"}', 'admin', '2023-12-07 21:09:00', 'admin', '2023-12-07 23:37:00', NULL, 1, 1, 3);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', '', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, NULL, 'input', NULL, 1, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (2, '1', 'game_name', '比赛名称', 'varchar(255)', 'String', 'gameName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 2, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (3, '1', 'game_type', '赛事类别', 'varchar(255)', 'String', 'gameType', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'znl_game_type', 3, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (4, '1', 'certificate_name', '证书名称', 'varchar(255)', 'String', 'certificateName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 4, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (5, '1', 'certificate_type', '证书类型', 'varchar(255)', 'String', 'certificateType', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'znl_certificate_type', 5, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (6, '1', 'certificate_number', '证书编号', 'varchar(255)', 'String', 'certificateNumber', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 6, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (7, '1', 'prize_giving', '颁奖单位', 'varchar(255)', 'String', 'prizeGiving', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 7, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (8, '1', 'acquisition_time', '获奖时间', 'datetime', 'Date', 'acquisitionTime', '0', '0', NULL, '1', '1', '1', '1', 'BETWEEN', 'datetime', NULL, 8, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (9, '1', 'certificate_rank', '获奖级别', 'varchar(255)', 'String', 'certificateRank', '0', '0', NULL, '1', '1', '1', '1', NULL, 'select', 'znl_certificate_rank', 9, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (10, '1', 'game_munber', '参赛成员', 'varchar(255)', 'String', 'gameMunber', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 10, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (11, '1', 'game_student_number', '参赛人员学号', 'varchar(255)', 'String', 'gameStudentNumber', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'input', NULL, 11, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (12, '1', 'game_student_college', '所属学院', 'varchar(255)', 'String', 'gameStudentCollege', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 12, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (13, '1', 'guide_teacher', '老师名字', 'varchar(255)', 'String', 'guideTeacher', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 13, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (14, '1', 'production_name', '作品名称', 'varchar(255)', 'String', 'productionName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', NULL, 14, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (15, '1', 'game_summary', '参赛总结', 'text', 'String', 'gameSummary', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'textarea', NULL, 15, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (16, '1', 'game_materials', '参赛材料', 'text', 'String', 'gameMaterials', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'textarea', NULL, 16, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (17, '1', 'game_address', '参赛地址', 'varchar(255)', 'String', 'gameAddress', '0', '0', NULL, '1', '1', '1', '1', NULL, 'input', NULL, 17, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (18, '1', 'certificate_front', '证书正面', 'varchar(255)', 'String', 'certificateFront', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'input', NULL, 18, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (19, '1', 'certificate_reverse', '证书反面', 'varchar(255)', 'String', 'certificateReverse', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'input', NULL, 19, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (20, '1', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, NULL, 'input', NULL, 20, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (21, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, NULL, 'datetime', NULL, 21, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (22, '1', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, NULL, 'input', NULL, 22, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:00');
INSERT INTO `gen_table_column` VALUES (23, '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, NULL, 'datetime', NULL, 23, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:01');
INSERT INTO `gen_table_column` VALUES (24, '1', 'remark', '备注', 'varchar(1000)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, NULL, 'textarea', NULL, 24, 'admin', '2023-12-07 21:09:00', NULL, '2023-12-07 23:37:01');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `blob_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400194C6A6176612F74696D652F4C6F63616C4461746554696D653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000D6A6176612E74696D652E536572955D84BA1B2248B20C00007870770A05000007E70C071318F878707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400194C6A6176612F74696D652F4C6F63616C4461746554696D653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000D6A6176612E74696D652E536572955D84BA1B2248B20C00007870770A05000007E70C071318F878707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400194C6A6176612F74696D652F4C6F63616C4461746554696D653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000D6A6176612E74696D652E536572955D84BA1B2248B20C00007870770A05000007E70C071318F878707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'DESKTOP-CL948K41710060931488', 1710062920950, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `int_prop_1` int(11) NULL DEFAULT NULL,
  `int_prop_2` int(11) NULL DEFAULT NULL,
  `long_prop_1` bigint(20) NULL DEFAULT NULL,
  `long_prop_2` bigint(20) NULL DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `next_fire_time` bigint(13) NULL DEFAULT NULL,
  `prev_fire_time` bigint(13) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) NULL DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `misfire_instr` smallint(2) NULL DEFAULT NULL,
  `job_data` blob NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1710060940000, -1, 5, 'PAUSED', 'CRON', 1710060931000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1710060945000, -1, 5, 'PAUSED', 'CRON', 1710060932000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1710060940000, -1, 5, 'PAUSED', 'CRON', 1710060932000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2023-12-07 19:24:06', 'admin', '2023-12-07 19:24:06', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2023-12-07 19:24:06', 'admin', '2023-12-07 19:24:06', '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2023-12-07 19:24:06', 'admin', '2023-12-07 19:24:06', '深色主题theme-dark，浅色主题theme-light');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门ID',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '若依科技', 0, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', 'admin', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '深圳总公司', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15588888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', 'admin', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '研发部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '市场部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '测试部门', 3, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '财务部门', 4, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', '运维部门', 5, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (110, 100, '0,100', '115', 555, '打算的撒啊啊', NULL, NULL, '1', '2', 'test', '2023-12-07 19:24:06', 'admin', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (111, 100, '0,100', '杭州分公司', 33, NULL, NULL, NULL, '0', '0', 'test', '2023-12-07 19:24:06', 'admin', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (112, 110, '0,100,110', '1515', 1, NULL, NULL, NULL, '0', '2', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (113, 112, '0,100,110,112', '大大撒啊', 1, NULL, NULL, NULL, '0', '2', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (114, 113, '0,100,110,112,113', '2434343', 1, NULL, NULL, NULL, '0', '2', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (115, 114, '0,100,110,112,113,114', '2222', 1, NULL, NULL, NULL, '0', '2', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (116, 115, '0,100,110,112,113,114,115', '微微儿', 1, NULL, NULL, NULL, '0', '2', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (117, 116, '0,100,110,112,113,114,115,116', '1111', 1, NULL, NULL, NULL, '0', '2', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (118, 117, '0,100,110,112,113,114,115,116,117', '23232', 1, NULL, NULL, NULL, '0', '2', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (119, 111, '0,100,111', '研发一部', 1, NULL, NULL, NULL, '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (120, 111, '0,100,111', '研发二部', 2, NULL, NULL, NULL, '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (121, 111, '0,100,111', '测试部门', 3, NULL, NULL, NULL, '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (122, 111, '0,100,111', '服务运维', 4, NULL, NULL, NULL, '0', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06');
INSERT INTO `sys_dept` VALUES (123, 111, '0,100,111', '售后维护', 6, '大大大啊', '15684848888', '111@sss.com', '1', '0', 'admin', '2023-12-07 19:24:06', 'admin', '2023-12-07 19:24:06');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 121 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2023-12-07 19:24:06', '', '2023-12-07 19:24:06', '停用状态');
INSERT INTO `sys_dict_data` VALUES (102, 1, '个人专利/软著证书', '1', 'znl_certificate_type', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:47:09', '', NULL, '个人专利、软著');
INSERT INTO `sys_dict_data` VALUES (103, 2, '个人职业证书', '2', 'znl_certificate_type', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:47:27', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (104, 3, '个人荣誉证书', '3', 'znl_certificate_type', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:47:56', '', NULL, '比如三好学生证书等');
INSERT INTO `sys_dict_data` VALUES (105, 4, '个人培训证书', '4', 'znl_certificate_type', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:48:26', '', NULL, '参加各种培训获得的证书');
INSERT INTO `sys_dict_data` VALUES (106, 1, '特等奖', '0', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:49:27', 'admin', '2023-12-07 21:49:48', NULL);
INSERT INTO `sys_dict_data` VALUES (107, 2, '一等奖', '1', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:50:05', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (108, 3, '二等奖', '2', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:50:23', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (109, 4, '三等奖', '3', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:50:40', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (110, 5, '优秀奖', '4', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:51:02', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (111, 6, '入围奖', '5', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:51:15', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (112, 1, '金奖', '11', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:51:36', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (113, 2, '银奖', '12', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:51:51', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (114, 3, '铜奖', '13', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:52:05', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (115, 7, '优秀指导老师', '6', 'znl_certificate_rank', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:52:28', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (117, 1, '国家级', '1', 'znl_game_type', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:55:48', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (118, 2, '省部级', '2', 'znl_game_type', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:56:09', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (119, 3, '地市级', '3', 'znl_game_type', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:56:25', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (120, 4, '校级', '4', 'znl_game_type', NULL, NULL, 'N', '0', 'admin', '2023-12-07 21:56:42', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-20 16:11:22', '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (12, '证书类型', 'znl_certificate_type', '0', 'admin', '2023-12-07 21:43:46', '', NULL, '证书类型');
INSERT INTO `sys_dict_type` VALUES (13, '证书获奖级别', 'znl_certificate_rank', '0', 'admin', '2023-12-07 21:44:05', '', NULL, NULL);
INSERT INTO `sys_dict_type` VALUES (14, '赛事类别', 'znl_game_type', '0', 'admin', '2023-12-07 21:44:22', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) NULL DEFAULT NULL COMMENT '访问时间',
  `token` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '登录token',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (1, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-07 19:37:04', 'ee5f87ad-3b7d-4210-b9c6-42187e8572f9');
INSERT INTO `sys_logininfor` VALUES (2, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '1', '验证码已失效', '2023-12-11 09:18:03', NULL);
INSERT INTO `sys_logininfor` VALUES (3, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-11 09:18:07', '649ce505-497e-44f4-976c-af555fef4bb9');
INSERT INTO `sys_logininfor` VALUES (4, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-18 16:26:33', '8234b2b0-944f-4a0d-bec5-2d25f7556584');
INSERT INTO `sys_logininfor` VALUES (5, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-18 22:31:32', '0c442629-500c-4568-a826-e2f08850eba8');
INSERT INTO `sys_logininfor` VALUES (6, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '退出成功', '2023-12-22 14:54:03', '0c442629-500c-4568-a826-e2f08850eba8');
INSERT INTO `sys_logininfor` VALUES (7, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '退出成功', '2023-12-22 14:54:03', '0c442629-500c-4568-a826-e2f08850eba8');
INSERT INTO `sys_logininfor` VALUES (8, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '退出成功', '2023-12-22 14:54:03', '0c442629-500c-4568-a826-e2f08850eba8');
INSERT INTO `sys_logininfor` VALUES (9, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-22 14:54:13', 'c6206dc7-f33c-4c94-89e9-3d92aac11333');
INSERT INTO `sys_logininfor` VALUES (10, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-24 20:37:36', '785561f8-f6dd-4d25-bd23-1bea52b323c7');
INSERT INTO `sys_logininfor` VALUES (11, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '1', '验证码错误', '2023-12-24 22:12:23', NULL);
INSERT INTO `sys_logininfor` VALUES (12, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-24 22:12:26', 'b5249178-401d-498c-b4f1-82b90ca19bb7');
INSERT INTO `sys_logininfor` VALUES (13, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '退出成功', '2023-12-24 22:19:53', 'b5249178-401d-498c-b4f1-82b90ca19bb7');
INSERT INTO `sys_logininfor` VALUES (14, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-24 22:20:08', '30425397-02a3-43a6-af4e-82c854652c4f');
INSERT INTO `sys_logininfor` VALUES (15, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2023-12-26 17:45:26', '92468b15-ba0b-42ba-8917-eb277b93f10e');
INSERT INTO `sys_logininfor` VALUES (16, 'admin', '127.0.0.1', '内网IP', 'Firefox 11', 'Windows 10', '0', '登录成功', '2024-03-10 17:00:51', '1fbaf68f-8e72-4610-945a-b7e997628848');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '组件路径',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2013 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, 1, 0, 'M', '0', '0', '', 'system', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, 'monitor', NULL, 1, 0, 'M', '0', '0', '', 'monitor', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, 'tool', NULL, 1, 0, 'M', '0', '0', '', 'tool', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '系统工具目录');
INSERT INTO `sys_menu` VALUES (4, '若依官网', 0, 4, 'http://ruoyi.vip', NULL, 0, 0, 'M', '0', '0', '', 'github', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '若依官网地址');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, 'user', 'system/user/index', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, 'role', 'system/role/index', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, 'menu', 'system/menu/index', 1, 0, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, 'dept', 'system/dept/index', 1, 0, 'C', '0', '0', 'system:dept:list', 'table', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, 'post', 'system/post/index', 1, 0, 'C', '0', '0', 'system:post:list', 'people', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, 'dict', 'system/dict/index', 1, 0, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, 'config', 'system/config/index', 1, 0, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, 'notice', 'system/notice/index', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, 'log', '', 1, 0, 'M', '0', '0', '', 'logininfor', 'admin', '2023-12-07 19:24:07', 'admin', '2022-07-28 08:16:18', '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, 'online', 'monitor/online/index', 1, 0, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, 'job', 'monitor/job/index', 1, 0, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, 'druid', 'monitor/druid/index', 1, 0, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, 'server', 'monitor/server/index', 1, 0, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, 'cache', 'monitor/cache/index', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, 'build', 'tool/build/index', 1, 0, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, 'gen', 'tool/gen/index', 1, 0, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2023-12-07 19:24:07', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, 'swagger', 'tool/swagger/index', 1, 0, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, 'operlog', 'monitor/operlog/index', 1, 0, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, 'logininfor', 'monitor/logininfor/index', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1019, '修改', 103, 3, '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1020, '删除', 103, 4, '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1021, '岗位查询', 104, 1, '', '', 1, 0, 'F', '0', '0', 'system:post:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1022, '岗位新增', 104, 2, '', '', 1, 0, 'F', '0', '0', 'system:post:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1023, '岗位修改', 104, 3, '', '', 1, 0, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1024, '岗位删除', 104, 4, '', '', 1, 0, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1025, '岗位导出', 104, 5, '', '', 1, 0, 'F', '0', '0', 'system:post:export', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1026, '字典查询', 105, 1, '#', '', 1, 0, 'F', '0', '0', 'system:dict:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1027, '字典新增', 105, 2, '#', '', 1, 0, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1028, '字典修改', 105, 3, '#', '', 1, 0, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1029, '字典删除', 105, 4, '#', '', 1, 0, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1030, '字典导出', 105, 5, '#', '', 1, 0, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1031, '参数查询', 106, 1, '#', '', 1, 0, 'F', '0', '0', 'system:config:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1032, '参数新增', 106, 2, '#', '', 1, 0, 'F', '0', '0', 'system:config:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1033, '参数修改', 106, 3, '#', '', 1, 0, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1034, '参数删除', 106, 4, '#', '', 1, 0, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1035, '参数导出', 106, 5, '#', '', 1, 0, 'F', '0', '0', 'system:config:export', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1040, '操作查询', 500, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1041, '操作删除', 500, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1046, '在线查询', 109, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1047, '批量强退', 109, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1048, '单条强退', 109, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1049, '任务查询', 110, 1, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1050, '任务新增', 110, 2, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1051, '任务修改', 110, 3, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1052, '任务删除', 110, 4, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1053, '状态修改', 110, 5, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1054, '任务导出', 110, 7, '#', '', 1, 0, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1055, '生成查询', 115, 1, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1056, '生成修改', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1057, '生成删除', 115, 3, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1058, '导入代码', 115, 2, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1059, '预览代码', 115, 4, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1060, '生成代码', 115, 5, '#', '', 1, 0, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (1061, '缓存列表', 2, 6, 'cacheList', 'monitor/cache/list', 1, 0, 'C', '0', '0', 'monitor:cache:list', 'list', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '');
INSERT INTO `sys_menu` VALUES (2000, '证书管理', 0, 2, 'commendation', NULL, 1, 0, 'M', '0', '0', '', 'excel', 'admin', '2023-12-07 21:35:34', 'admin', '2023-12-07 23:34:37', '');
INSERT INTO `sys_menu` VALUES (2007, '证书信息管理', 2000, 1, 'info', 'commendation/info/index', 1, 0, 'C', '0', '0', 'commendation:info:list', '#', 'admin', '2023-12-07 23:39:53', '', '2023-12-07 23:39:53', '证书信息管理菜单');
INSERT INTO `sys_menu` VALUES (2008, '证书信息管理查询', 2007, 1, '#', '', 1, 0, 'F', '0', '0', 'commendation:info:query', '#', 'admin', '2023-12-07 23:39:53', '', '2023-12-07 23:39:53', '');
INSERT INTO `sys_menu` VALUES (2009, '证书信息管理新增', 2007, 2, '#', '', 1, 0, 'F', '0', '0', 'commendation:info:add', '#', 'admin', '2023-12-07 23:39:53', '', '2023-12-07 23:39:53', '');
INSERT INTO `sys_menu` VALUES (2010, '证书信息管理修改', 2007, 3, '#', '', 1, 0, 'F', '0', '0', 'commendation:info:edit', '#', 'admin', '2023-12-07 23:39:53', '', '2023-12-07 23:39:53', '');
INSERT INTO `sys_menu` VALUES (2011, '证书信息管理删除', 2007, 4, '#', '', 1, 0, 'F', '0', '0', 'commendation:info:remove', '#', 'admin', '2023-12-07 23:39:53', '', '2023-12-07 23:39:53', '');
INSERT INTO `sys_menu` VALUES (2012, '证书信息管理导出', 2007, 5, '#', '', 1, 0, 'F', '0', '0', 'commendation:info:export', '#', 'admin', '2023-12-07 23:39:53', '', '2023-12-07 23:39:53', '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '请求参数',
  `json_result` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL COMMENT '错误消息',
  `oper_time` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 178 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (1, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dict/type/11', '127.0.0.1', '内网IP', '{ids=11}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"2dZaTb4crZoYUiOoBNKai\"}', 0, NULL, '2023-12-07 19:38:01');
INSERT INTO `sys_oper_log` VALUES (2, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'znl_certificate_info', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"3-MmPXruTJ750xTSANvwi\"}', 0, NULL, '2023-12-07 21:09:00');
INSERT INTO `sys_oper_log` VALUES (3, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zhangch\",\"functionName\":\"证书信息管理\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"jdbcType\":\"BIGINT\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"\",\"updateTime\":1701954540000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1701954540000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"GameName\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"比赛名称\",\"isQuery\":\"1\",\"updateTime\":1701954540000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_name\"},{\"capJavaField\":\"GameType\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"赛事类别\",\"isQuery\":\"1\",\"updateTime\":1701954540000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_type\"},{\"capJavaField\":\"CertificateName\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"certificateName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"证书名称\",\"isQuery\":\"1\",\"updateTime\":1701954540000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"S', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"-ZSNAdN0mIgT0roCsqzkh\"}', 0, NULL, '2023-12-07 21:21:53');
INSERT INTO `sys_oper_log` VALUES (4, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"excel\",\"orderNum\":\"2\",\"menuName\":\"证书管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"1\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"9Ytd-FmJJL_hk-AvFPpMY\"}', 0, NULL, '2023-12-07 21:35:34');
INSERT INTO `sys_oper_log` VALUES (5, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"excel\",\"orderNum\":\"2\",\"menuName\":\"证书管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"certificate\",\"children\":[],\"createTime\":1701956134000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"5VmEAPQG5VUmTqYH27V30\"}', 0, NULL, '2023-12-07 21:36:24');
INSERT INTO `sys_oper_log` VALUES (6, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zhangch\",\"functionName\":\"证书信息管理\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"jdbcType\":\"BIGINT\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"\",\"updateTime\":1701955313000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1701954540000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"GameName\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"比赛名称\",\"isQuery\":\"1\",\"updateTime\":1701955313000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_name\"},{\"capJavaField\":\"GameType\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"赛事类别\",\"isQuery\":\"1\",\"updateTime\":1701955313000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_type\"},{\"capJavaField\":\"CertificateName\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"certificateName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"证书名称\",\"isQuery\":\"1\",\"updateTime\":1701955313000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"S', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"aSj8S0C_n0SmrR349Zu2p\"}', 0, NULL, '2023-12-07 21:41:34');
INSERT INTO `sys_oper_log` VALUES (7, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"证书类型\",\"remark\":\"证书类型\",\"params\":{},\"dictType\":\"znl_certificate_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"6jNPYmdYD9pzzMHeEL-Zs\"}', 0, NULL, '2023-12-07 21:43:46');
INSERT INTO `sys_oper_log` VALUES (8, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"证书获奖级别\",\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"1o73S1B6BYwsME87ARfbI\"}', 0, NULL, '2023-12-07 21:44:05');
INSERT INTO `sys_oper_log` VALUES (9, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', 1, 'admin', NULL, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"赛事类别\",\"params\":{},\"dictType\":\"znl_game_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"6dbnsLG08QOZERozhY-fg\"}', 0, NULL, '2023-12-07 21:44:22');
INSERT INTO `sys_oper_log` VALUES (10, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"个人专利/软著证书\",\"dictValue\":\"1\",\"createBy\":\"admin\",\"dictSort\":1,\"remark\":\"个人专利、软著\",\"params\":{},\"dictType\":\"znl_certificate_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"oo0J2UyZUJRHJGK-_l89C\"}', 0, NULL, '2023-12-07 21:45:51');
INSERT INTO `sys_oper_log` VALUES (11, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"个人职业证书\",\"dictValue\":\"1\",\"createBy\":\"admin\",\"dictSort\":0,\"params\":{},\"dictType\":\"znl_certificate_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"pjfxDahp266YVKmIGpKX6\"}', 0, NULL, '2023-12-07 21:46:33');
INSERT INTO `sys_oper_log` VALUES (12, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictDataController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dict/data/100,101', '127.0.0.1', '内网IP', '{dictCodes=100,101}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"hesKwUpavdcOY0dYpRxqN\"}', 0, NULL, '2023-12-07 21:46:41');
INSERT INTO `sys_oper_log` VALUES (13, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"个人专利/软著证书\",\"dictValue\":\"1\",\"createBy\":\"admin\",\"dictSort\":1,\"remark\":\"个人专利、软著\",\"params\":{},\"dictType\":\"znl_certificate_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"klSfecxM15GcmzArfOCi8\"}', 0, NULL, '2023-12-07 21:47:09');
INSERT INTO `sys_oper_log` VALUES (14, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"个人职业证书\",\"dictValue\":\"2\",\"createBy\":\"admin\",\"dictSort\":2,\"params\":{},\"dictType\":\"znl_certificate_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"o9Skmxfh1axQsV7k_vUHz\"}', 0, NULL, '2023-12-07 21:47:27');
INSERT INTO `sys_oper_log` VALUES (15, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"个人荣誉证书\",\"dictValue\":\"3\",\"createBy\":\"admin\",\"dictSort\":3,\"remark\":\"比如三好学生证书等\",\"params\":{},\"dictType\":\"znl_certificate_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"yG9jBq9Hh790nCg5zXJKY\"}', 0, NULL, '2023-12-07 21:47:56');
INSERT INTO `sys_oper_log` VALUES (16, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"个人培训证书\",\"dictValue\":\"4\",\"createBy\":\"admin\",\"dictSort\":4,\"remark\":\"参加各种培训获得的证书\",\"params\":{},\"dictType\":\"znl_certificate_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"s1yQ2YFmgN76gbgkqMNQ4\"}', 0, NULL, '2023-12-07 21:48:26');
INSERT INTO `sys_oper_log` VALUES (17, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"1\",\"dictValue\":\"1\",\"createBy\":\"admin\",\"dictSort\":0,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"nSTuCa4XaxVdSa4WYF94R\"}', 0, NULL, '2023-12-07 21:49:27');
INSERT INTO `sys_oper_log` VALUES (18, '字典数据', 2, 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"特等奖\",\"dictValue\":\"0\",\"createBy\":\"admin\",\"isDefault\":\"N\",\"createTime\":1701956967000,\"dictCode\":106,\"updateBy\":\"admin\",\"dictSort\":1,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"bK2E8qOVUYTHu2B8-iEKD\"}', 0, NULL, '2023-12-07 21:49:48');
INSERT INTO `sys_oper_log` VALUES (19, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"一等奖\",\"dictValue\":\"1\",\"createBy\":\"admin\",\"dictSort\":2,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"vNN016U-gfZyiqwREODGI\"}', 0, NULL, '2023-12-07 21:50:05');
INSERT INTO `sys_oper_log` VALUES (20, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"二等奖\",\"dictValue\":\"2\",\"createBy\":\"admin\",\"dictSort\":3,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"XPIW7cNlK9270Y_kVZDsY\"}', 0, NULL, '2023-12-07 21:50:23');
INSERT INTO `sys_oper_log` VALUES (21, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"三等奖\",\"dictValue\":\"3\",\"createBy\":\"admin\",\"dictSort\":4,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"X5sU8Ua0VIVBhJuefvHYs\"}', 0, NULL, '2023-12-07 21:50:40');
INSERT INTO `sys_oper_log` VALUES (22, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"优秀奖\",\"dictValue\":\"4\",\"createBy\":\"admin\",\"dictSort\":5,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"YEag3wVKb-Y_q_wwN6i-A\"}', 0, NULL, '2023-12-07 21:51:02');
INSERT INTO `sys_oper_log` VALUES (23, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"入围奖\",\"dictValue\":\"5\",\"createBy\":\"admin\",\"dictSort\":6,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"J6q6_C6ASNcaoDjzJhUkP\"}', 0, NULL, '2023-12-07 21:51:15');
INSERT INTO `sys_oper_log` VALUES (24, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"金奖\",\"dictValue\":\"11\",\"createBy\":\"admin\",\"dictSort\":1,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"zvKSe4cKqU1MsUgazgo18\"}', 0, NULL, '2023-12-07 21:51:36');
INSERT INTO `sys_oper_log` VALUES (25, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"银奖\",\"dictValue\":\"12\",\"createBy\":\"admin\",\"dictSort\":2,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"WrwZ9KtvgwsSzEYPUi4sO\"}', 0, NULL, '2023-12-07 21:51:51');
INSERT INTO `sys_oper_log` VALUES (26, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"铜奖\",\"dictValue\":\"13\",\"createBy\":\"admin\",\"dictSort\":3,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"7tvWrR7x8oSMazopRSGk0\"}', 0, NULL, '2023-12-07 21:52:05');
INSERT INTO `sys_oper_log` VALUES (27, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"优秀指导老师\",\"dictValue\":\"6\",\"createBy\":\"admin\",\"dictSort\":7,\"params\":{},\"dictType\":\"znl_certificate_rank\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"b_o6HqHOy7kErO4VMbcgx\"}', 0, NULL, '2023-12-07 21:52:28');
INSERT INTO `sys_oper_log` VALUES (28, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"国家级\",\"dictValue\":\"1\",\"createBy\":\"admin\",\"dictSort\":1,\"params\":{},\"dictType\":\"znl_game_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"AsHCRAniB3hhAyJKxi5WN\"}', 0, NULL, '2023-12-07 21:55:20');
INSERT INTO `sys_oper_log` VALUES (29, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"国家级\",\"dictValue\":\"1\",\"createBy\":\"admin\",\"dictSort\":1,\"params\":{},\"dictType\":\"znl_game_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"K4K_vO12TDgjjjHj0Uwc1\"}', 0, NULL, '2023-12-07 21:55:48');
INSERT INTO `sys_oper_log` VALUES (30, '字典类型', 3, 'com.ruoyi.web.controller.system.SysDictDataController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dict/data/116', '127.0.0.1', '内网IP', '{dictCodes=116}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"esIJAsMarOGLwejaOAQcb\"}', 0, NULL, '2023-12-07 21:55:52');
INSERT INTO `sys_oper_log` VALUES (31, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"省部级\",\"dictValue\":\"2\",\"createBy\":\"admin\",\"dictSort\":2,\"params\":{},\"dictType\":\"znl_game_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"uqzcaOpXy389hRUZqa0CR\"}', 0, NULL, '2023-12-07 21:56:09');
INSERT INTO `sys_oper_log` VALUES (32, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"地市级\",\"dictValue\":\"3\",\"createBy\":\"admin\",\"dictSort\":3,\"params\":{},\"dictType\":\"znl_game_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"nzHVREpRubQVDefijMbit\"}', 0, NULL, '2023-12-07 21:56:25');
INSERT INTO `sys_oper_log` VALUES (33, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', 1, 'admin', NULL, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictLabel\":\"校级\",\"dictValue\":\"4\",\"createBy\":\"admin\",\"dictSort\":4,\"params\":{},\"dictType\":\"znl_game_type\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"ejkTn4FLOfONYzxEpv9-_\"}', 0, NULL, '2023-12-07 21:56:42');
INSERT INTO `sys_oper_log` VALUES (34, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zhangch\",\"functionName\":\"证书信息管理\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"jdbcType\":\"BIGINT\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"\",\"updateTime\":1701956494000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1701954540000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"GameName\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"比赛名称\",\"isQuery\":\"1\",\"updateTime\":1701956494000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_name\"},{\"capJavaField\":\"GameType\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"znl_game_type\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"赛事类别\",\"isQuery\":\"1\",\"updateTime\":1701956494000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_type\"},{\"capJavaField\":\"CertificateName\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"certificateName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"证书名称\",\"isQuery\":\"1\",\"updateTime\":1701956494000,\"sort\":4,\"list\":tru', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"XYHFYYGihbV8bnE6xRSf3\"}', 0, NULL, '2023-12-07 21:58:13');
INSERT INTO `sys_oper_log` VALUES (35, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zhangch\",\"functionName\":\"证书信息管理\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"jdbcType\":\"BIGINT\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"\",\"updateTime\":1701957493000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1701954540000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"GameName\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"比赛名称\",\"isQuery\":\"1\",\"updateTime\":1701957493000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_name\"},{\"capJavaField\":\"GameType\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"znl_game_type\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"赛事类别\",\"isQuery\":\"1\",\"updateTime\":1701957493000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_type\"},{\"capJavaField\":\"CertificateName\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"certificateName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"证书名称\",\"isQuery\":\"1\",\"updateTime\":1701957493000,\"sort\":4,\"list\":tru', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"dz0-tUOzdj3dR7E6o-TIp\"}', 0, NULL, '2023-12-07 22:01:00');
INSERT INTO `sys_oper_log` VALUES (36, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2023-12-07 22:01:24');
INSERT INTO `sys_oper_log` VALUES (37, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2023-12-07 22:36:30');
INSERT INTO `sys_oper_log` VALUES (38, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":\"1\",\"menuName\":\"证书信息管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"info\",\"component\":\"certificate/info/index\",\"children\":[],\"createTime\":1701957878000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"certificate:info:list\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"9njaOFuUeTiaOZItRxoWQ\"}', 0, NULL, '2023-12-07 22:52:12');
INSERT INTO `sys_oper_log` VALUES (39, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":\"1\",\"menuName\":\"证书信息管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"info\",\"component\":\"certificate_info/index\",\"children\":[],\"createTime\":1701957878000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"certificate:info:list\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"5rxznmFWa5JMCLFultpYs\"}', 0, NULL, '2023-12-07 23:15:28');
INSERT INTO `sys_oper_log` VALUES (40, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"excel\",\"orderNum\":\"2\",\"menuName\":\"证书管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"certificate_info\",\"children\":[],\"createTime\":1701956134000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"6RaKV8xZL4WyP0Fo_DpRb\"}', 0, NULL, '2023-12-07 23:30:51');
INSERT INTO `sys_oper_log` VALUES (41, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":\"1\",\"menuName\":\"证书信息管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"info\",\"component\":\"certificate_info/index\",\"children\":[],\"createTime\":1701957878000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"certificate:info:list\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"FI4rr3skjlV9GPiZuPthO\"}', 0, NULL, '2023-12-07 23:31:15');
INSERT INTO `sys_oper_log` VALUES (42, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"excel\",\"orderNum\":\"2\",\"menuName\":\"证书管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"commendation\",\"children\":[],\"createTime\":1701956134000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2000,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"b7tpIP2JPz9AJ0z2hvEOg\"}', 0, NULL, '2023-12-07 23:34:37');
INSERT INTO `sys_oper_log` VALUES (43, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2001', '127.0.0.1', '内网IP', '{menuId=2001}', '{\"code\":500,\"msg\":\"存在子菜单,不允许删除\",\"requestId\":\"zo8MJ5IFBUfx4XEHrL3ak\"}', 0, NULL, '2023-12-07 23:35:15');
INSERT INTO `sys_oper_log` VALUES (44, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2002', '127.0.0.1', '内网IP', '{menuId=2002}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"ltwkeRIbueGECL4ju1rAU\"}', 0, NULL, '2023-12-07 23:35:20');
INSERT INTO `sys_oper_log` VALUES (45, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2003', '127.0.0.1', '内网IP', '{menuId=2003}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"MiW6KMaYhwasZQl2nu9kZ\"}', 0, NULL, '2023-12-07 23:35:25');
INSERT INTO `sys_oper_log` VALUES (46, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2004', '127.0.0.1', '内网IP', '{menuId=2004}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"ZQ0hnhQfisbJiNXNTpJFs\"}', 0, NULL, '2023-12-07 23:35:30');
INSERT INTO `sys_oper_log` VALUES (47, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2005', '127.0.0.1', '内网IP', '{menuId=2005}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"9hmiGg1WLWYJR5fQic1I8\"}', 0, NULL, '2023-12-07 23:35:33');
INSERT INTO `sys_oper_log` VALUES (48, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2006', '127.0.0.1', '内网IP', '{menuId=2006}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"l3DKLw_4txZnRPGxAbYgi\"}', 0, NULL, '2023-12-07 23:35:36');
INSERT INTO `sys_oper_log` VALUES (49, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2001', '127.0.0.1', '内网IP', '{menuId=2001}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"76ai2QyANKr9Cy20lP-s2\"}', 0, NULL, '2023-12-07 23:35:42');
INSERT INTO `sys_oper_log` VALUES (50, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', 1, 'admin', NULL, '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zhangch\",\"functionName\":\"证书信息管理\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"required\":false,\"superColumn\":false,\"jdbcType\":\"BIGINT\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"\",\"updateTime\":1701957660000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1701954540000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"GameName\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"比赛名称\",\"isQuery\":\"1\",\"updateTime\":1701957660000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_name\"},{\"capJavaField\":\"GameType\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"znl_game_type\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"gameType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"赛事类别\",\"isQuery\":\"1\",\"updateTime\":1701957660000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1701954540000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"game_type\"},{\"capJavaField\":\"CertificateName\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"required\":false,\"superColumn\":false,\"jdbcType\":\"VARCHAR\",\"isInsert\":\"1\",\"javaField\":\"certificateName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"证书名称\",\"isQuery\":\"1\",\"updateTime\":1701957660000,\"sort\":4,\"list\":tru', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"Ol_ekV72wx9LGvNCZZfrp\"}', 0, NULL, '2023-12-07 23:37:01');
INSERT INTO `sys_oper_log` VALUES (51, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', 0, NULL, '2023-12-07 23:37:50');
INSERT INTO `sys_oper_log` VALUES (52, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"createTime\":1702024031692,\"id\":48,\"params\":{}}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"4nJ627_EI_6N8TSZKEqa8\"}', 0, NULL, '2023-12-08 16:27:11');
INSERT INTO `sys_oper_log` VALUES (53, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"createTime\":1702024036339,\"id\":49,\"params\":{}}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"6kCmveZ1JvPGmhiswWjVU\"}', 0, NULL, '2023-12-08 16:27:16');
INSERT INTO `sys_oper_log` VALUES (54, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/48,49', '127.0.0.1', '内网IP', '{ids=48,49}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"G4mXVp-CK1u_vgkOoeJmC\"}', 0, NULL, '2023-12-08 17:57:42');
INSERT INTO `sys_oper_log` VALUES (55, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-08 18:59:30');
INSERT INTO `sys_oper_log` VALUES (56, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-08 19:19:15');
INSERT INTO `sys_oper_log` VALUES (57, '用户管理', 5, 'com.ruoyi.web.controller.system.SysUserController.exportByStream()', 'POST', 1, 'admin', NULL, '/system/user/exportByStream', '127.0.0.1', '内网IP', '{\"admin\":false,\"params\":{}}', 'null', 0, NULL, '2023-12-08 19:20:04');
INSERT INTO `sys_oper_log` VALUES (58, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-08 19:50:33');
INSERT INTO `sys_oper_log` VALUES (59, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'GET', 1, 'admin', NULL, '/commendation/info/exportIds/46', '127.0.0.1', '内网IP', '{ids=46}', 'null', 0, NULL, '2023-12-08 20:10:59');
INSERT INTO `sys_oper_log` VALUES (60, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-08 20:13:09');
INSERT INTO `sys_oper_log` VALUES (61, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'GET', 1, 'admin', NULL, '/commendation/info/exportIds/47', '127.0.0.1', '内网IP', '{ids=47}', 'null', 0, NULL, '2023-12-08 20:13:20');
INSERT INTO `sys_oper_log` VALUES (62, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'GET', 1, 'admin', NULL, '/commendation/info/exportIds/47', '127.0.0.1', '内网IP', '{ids=47}', 'null', 0, NULL, '2023-12-08 20:13:32');
INSERT INTO `sys_oper_log` VALUES (63, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'GET', 1, 'admin', NULL, '/commendation/info/exportIds/47', '127.0.0.1', '内网IP', '{ids=47}', 'null', 0, NULL, '2023-12-08 20:15:09');
INSERT INTO `sys_oper_log` VALUES (64, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-08 20:28:09');
INSERT INTO `sys_oper_log` VALUES (65, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'POST', 1, 'admin', NULL, '/commendation/info/exportIds', '127.0.0.1', '内网IP', '', 'null', 0, NULL, '2023-12-08 20:49:08');
INSERT INTO `sys_oper_log` VALUES (66, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'POST', 1, 'admin', NULL, '/commendation/info/exportIds', '127.0.0.1', '内网IP', '{\"ids\":[\"46\"]}', 'null', 0, NULL, '2023-12-08 20:55:48');
INSERT INTO `sys_oper_log` VALUES (67, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'POST', 1, 'admin', NULL, '/commendation/info/exportIds', '127.0.0.1', '内网IP', '{\"ids\":[\"47\"]}', 'null', 0, NULL, '2023-12-08 20:58:24');
INSERT INTO `sys_oper_log` VALUES (68, '证书信息管理', 2, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.edit()', 'PUT', 1, 'admin', NULL, '/commendation/info/edit', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"gameAddress\":\"\",\"gameSummary\":\"\",\"certificateName\":\"试验-一等奖\",\"updateTime\":1702040534361,\"gameStudentCollege\":\"建筑与电气工程学院\",\"params\":{},\"certificateRank\":\"4\",\"gameName\":\"试验2\",\"certificateNumber\":\"CRAIC\",\"gameMaterials\":\"\",\"gameStudentNumber\":\"201300046\",\"acquisitionTime\":1640966400000,\"gameMember\":\"曾妍\",\"id\":47,\"productionName\":\"\",\"guideTeacher\":\"曾妍\",\"certificateType\":\"3\",\"prizeGiving\":\"试验\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"veoA1byunbiVDTF2KupMM\"}', 0, NULL, '2023-12-08 21:02:14');
INSERT INTO `sys_oper_log` VALUES (69, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'POST', 1, 'admin', NULL, '/commendation/info/exportIds', '127.0.0.1', '内网IP', '{\"ids\":[\"47\"]}', 'null', 0, NULL, '2023-12-08 21:02:29');
INSERT INTO `sys_oper_log` VALUES (70, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-08 21:02:39');
INSERT INTO `sys_oper_log` VALUES (71, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'POST', 1, 'admin', NULL, '/commendation/info/exportIds', '127.0.0.1', '内网IP', '{\"ids\":[\"47\"]}', 'null', 0, NULL, '2023-12-08 21:05:00');
INSERT INTO `sys_oper_log` VALUES (72, '角色管理', 5, 'com.ruoyi.web.controller.system.SysRoleController.responseStream()', 'POST', 1, 'admin', NULL, '/system/role/exportByStream', '127.0.0.1', '内网IP', '{\"deptCheckStrictly\":false,\"flag\":false,\"menuCheckStrictly\":false,\"admin\":false,\"params\":{}}', 'null', 0, NULL, '2023-12-08 21:08:27');
INSERT INTO `sys_oper_log` VALUES (73, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-08 21:09:05');
INSERT INTO `sys_oper_log` VALUES (74, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'POST', 1, 'admin', NULL, '/commendation/info/exportIds', '127.0.0.1', '内网IP', '{\"ids\":[\"47\"]}', 'null', 0, NULL, '2023-12-08 21:09:21');
INSERT INTO `sys_oper_log` VALUES (75, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"gameName\":\"test\",\"createTime\":1702040992198,\"certificateName\":\"test\",\"acquisitionTime\":1703001600000,\"id\":50,\"params\":{},\"certificateRank\":\"0\",\"certificateType\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"esqG-SFqXGooD3y3m1Yup\"}', 0, NULL, '2023-12-08 21:09:52');
INSERT INTO `sys_oper_log` VALUES (76, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"createTime\":1702075768723,\"acquisitionTime\":1702310400000,\"id\":51,\"params\":{},\"certificateRank\":\"0\",\"certificateFront\":\"http://localhost:8300/profile/upload/2023/12/09/profile_20231209064813A010.jpg\",\"certificateType\":\"2\",\"prizeGiving\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"m1qdB_1_RC2fATrDXtNJX\"}', 0, NULL, '2023-12-09 06:49:28');
INSERT INTO `sys_oper_log` VALUES (77, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"createTime\":1702257977331,\"id\":52,\"params\":{}}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"l4iAlPGCDi1qhMxUmLJms\"}', 0, NULL, '2023-12-11 09:26:17');
INSERT INTO `sys_oper_log` VALUES (78, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"createTime\":1702258002791,\"id\":53,\"params\":{}}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"_qwa-2RTSQIjGUInWdH2U\"}', 0, NULL, '2023-12-11 09:26:42');
INSERT INTO `sys_oper_log` VALUES (79, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/53', '127.0.0.1', '内网IP', '{ids=53}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"k-toLytcz4r3BgPaMnATG\"}', 0, NULL, '2023-12-11 09:26:49');
INSERT INTO `sys_oper_log` VALUES (80, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/52', '127.0.0.1', '内网IP', '{ids=52}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"cnt4x0-hOWfUZ3Qfg4zyY\"}', 0, NULL, '2023-12-11 09:26:54');
INSERT INTO `sys_oper_log` VALUES (81, '证书信息管理', 2, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.edit()', 'PUT', 1, 'admin', NULL, '/commendation/info/edit', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"createTime\":1702075769000,\"updateTime\":1702258033338,\"acquisitionTime\":1702310400000,\"id\":51,\"params\":{},\"certificateRank\":\"0\",\"certificateFront\":\"http://localhost:8300/profile/upload/2023/12/11/home-icon_20231211092707A004.png\",\"certificateType\":\"2\",\"prizeGiving\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"2Le7-C2D5UdnCChr9tWqM\"}', 0, NULL, '2023-12-11 09:27:13');
INSERT INTO `sys_oper_log` VALUES (82, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/51', '127.0.0.1', '内网IP', '{ids=51}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"T8e2LDHaGO9v47j-1nqbx\"}', 0, NULL, '2023-12-11 09:27:21');
INSERT INTO `sys_oper_log` VALUES (83, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"createTime\":1702258797544,\"id\":54,\"params\":{}}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"7sA-3Rxn0AV2IrbMkcywj\"}', 0, NULL, '2023-12-11 09:39:57');
INSERT INTO `sys_oper_log` VALUES (84, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/54', '127.0.0.1', '内网IP', '{ids=54}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"LlOy_uQgThIflVUkMJJxt\"}', 0, NULL, '2023-12-11 09:40:03');
INSERT INTO `sys_oper_log` VALUES (85, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"gameAddress\":\"南宁市查抄抄抄\",\"params\":{},\"certificateRank\":\"11\",\"certificateFront\":\"http://localhost:8300/profile/upload/2023/12/11/icon_20231211150032A017.jpg\",\"certificateNumber\":\"sdfsaf\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2023/12/11/home-icon_20231211150036A018.png\",\"createTime\":1702278078048,\"acquisitionTime\":1701705600000,\"id\":55,\"productionName\":\"全额付打发打发\",\"certificateType\":\"2\",\"prizeGiving\":\"市政府\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"DRy0D9fTk5UPAOUmGEOpb\"}', 0, NULL, '2023-12-11 15:01:18');
INSERT INTO `sys_oper_log` VALUES (86, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/55', '127.0.0.1', '内网IP', '{ids=55}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"lZzlxmLxhwACdpfI2Bi6X\"}', 0, NULL, '2023-12-11 15:09:22');
INSERT INTO `sys_oper_log` VALUES (87, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"gameAddress\":\"天上\",\"gameSummary\":\"好，这场比赛激动人心\",\"certificateName\":\"航天飞行证书\",\"gameStudentCollege\":\"124421\",\"params\":{},\"certificateRank\":\"11\",\"certificateFront\":\"http://localhost:8300/profile/upload/2023/12/11/icon_20231211153134A019.jpg\",\"gameName\":\"中国航天大赛\",\"certificateNumber\":\"1234qwer\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2023/12/11/profile_20231211153140A020.jpg\",\"createTime\":1702279955279,\"gameStudentNumber\":\"1234124\",\"acquisitionTime\":1701878400000,\"gameMember\":\"张重虎\\\\ddd\",\"id\":56,\"productionName\":\"航天飞行员王牌\",\"guideTeacher\":\"124421\",\"certificateType\":\"3\",\"prizeGiving\":\"中国航天\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"wbUAf9Ytxrd4hyRU2MLQn\"}', 0, NULL, '2023-12-11 15:32:35');
INSERT INTO `sys_oper_log` VALUES (88, '证书信息管理', 2, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.edit()', 'PUT', 1, 'admin', NULL, '/commendation/info/edit', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"gameAddress\":\"\",\"gameSummary\":\"\",\"certificateName\":\"试验-一等奖\",\"updateTime\":1702286445448,\"gameStudentCollege\":\"建筑与电气工程学院\",\"params\":{},\"certificateRank\":\"4\",\"certificateFront\":\"http://localhost:8300/profile/upload/2023/12/11/home-icon_20231211172037A022.png\",\"gameName\":\"试验\",\"certificateNumber\":\"CRAIC\",\"gameMaterials\":\"\",\"gameStudentNumber\":\"201300046\",\"acquisitionTime\":1640966400000,\"gameMember\":\"曾妍\",\"id\":46,\"productionName\":\"111\",\"guideTeacher\":\"曾妍\",\"certificateType\":\"3\",\"prizeGiving\":\"试验\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"v4Iv4_8ivJ3DZDXxmDP6m\"}', 0, NULL, '2023-12-11 17:20:45');
INSERT INTO `sys_oper_log` VALUES (89, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"gameAddress\":\"111\",\"certificateName\":\"111\",\"params\":{},\"certificateRank\":\"1\",\"certificateFront\":\"http://localhost:8300/profile/upload/2023/12/11/icon_20231211175235A025.jpg\",\"gameName\":\"111\",\"certificateNumber\":\"11\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2023/12/11/home-icon_20231211175240A026.png\",\"createTime\":1702288365677,\"acquisitionTime\":1701792000000,\"gameMember\":\"1\",\"id\":57,\"productionName\":\"111\",\"certificateType\":\"3\",\"prizeGiving\":\"11\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"HONrtiHsDxUVrkZu4F06X\"}', 0, NULL, '2023-12-11 17:52:45');
INSERT INTO `sys_oper_log` VALUES (90, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"gameName\":\"1\",\"createTime\":1702288383629,\"certificateName\":\"1\",\"acquisitionTime\":1701792000000,\"id\":58,\"params\":{},\"productionName\":\"1\",\"certificateRank\":\"1\",\"certificateType\":\"3\",\"prizeGiving\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"K0iMkhXHEm89CHBZ6Trcr\"}', 0, NULL, '2023-12-11 17:53:03');
INSERT INTO `sys_oper_log` VALUES (91, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"gameName\":\"1\",\"createTime\":1702288407071,\"certificateName\":\"1\",\"acquisitionTime\":1701705600000,\"id\":59,\"params\":{},\"productionName\":\"1\",\"certificateRank\":\"11\",\"certificateType\":\"2\",\"prizeGiving\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"tmcMhVmp-MNNrl0JtkWfl\"}', 0, NULL, '2023-12-11 17:53:27');
INSERT INTO `sys_oper_log` VALUES (92, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"gameName\":\"1\",\"createTime\":1702288428123,\"certificateName\":\"1\",\"acquisitionTime\":1702396800000,\"id\":60,\"params\":{},\"productionName\":\"111\",\"certificateRank\":\"11\",\"certificateType\":\"2\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"knxiFvzlKAVkf5eyBPJ69\"}', 0, NULL, '2023-12-11 17:53:48');
INSERT INTO `sys_oper_log` VALUES (93, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"gameName\":\"1\",\"createTime\":1702288445527,\"certificateName\":\"1\",\"acquisitionTime\":1703001600000,\"id\":61,\"params\":{},\"productionName\":\"1\",\"certificateRank\":\"11\",\"certificateType\":\"2\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"ZyncpCZB3yYRgm_eYCq9B\"}', 0, NULL, '2023-12-11 17:54:05');
INSERT INTO `sys_oper_log` VALUES (94, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"gameName\":\"11\",\"certificateNumber\":\"1\",\"createTime\":1702288464185,\"certificateName\":\"11\",\"acquisitionTime\":1701705600000,\"id\":62,\"params\":{},\"productionName\":\"111\",\"certificateRank\":\"11\",\"certificateType\":\"2\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"4HTDNp-wrfH0SSYmxJe_8\"}', 0, NULL, '2023-12-11 17:54:24');
INSERT INTO `sys_oper_log` VALUES (95, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"3\",\"gameName\":\"11\",\"createTime\":1702288477044,\"certificateName\":\"111\",\"acquisitionTime\":1701705600000,\"id\":63,\"params\":{},\"productionName\":\"111\",\"certificateRank\":\"12\",\"certificateType\":\"3\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"pXg9GWspcfJ-Q0LPeiQPk\"}', 0, NULL, '2023-12-11 17:54:37');
INSERT INTO `sys_oper_log` VALUES (96, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"3\",\"gameName\":\"11\",\"createTime\":1702288504939,\"certificateName\":\"11\",\"id\":64,\"params\":{},\"productionName\":\"111\",\"certificateRank\":\"11\",\"certificateType\":\"2\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"MS4S896tDNDleGVZtYX0t\"}', 0, NULL, '2023-12-11 17:55:04');
INSERT INTO `sys_oper_log` VALUES (97, '证书信息管理', 2, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.edit()', 'PUT', 1, 'admin', NULL, '/commendation/info/edit', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"gameAddress\":\"\",\"gameSummary\":\"\",\"certificateName\":\"试验-一等奖\",\"updateTime\":1702891514702,\"gameStudentCollege\":\"建筑与电气工程学院\",\"params\":{},\"certificateRank\":\"4\",\"certificateFront\":\"http://localhost:8300/profile/upload/2023/12/11/home-icon_20231211172037A022.png\",\"gameName\":\"试验1\",\"certificateNumber\":\"CRAIC\",\"gameMaterials\":\"\",\"gameStudentNumber\":\"201300046\",\"acquisitionTime\":1640966400000,\"gameMember\":\"曾妍\",\"id\":46,\"productionName\":\"111\",\"guideTeacher\":\"曾妍\",\"certificateType\":\"3\",\"prizeGiving\":\"试验\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"z6ZbCE-BFGF8-sbwOZMSi\"}', 0, NULL, '2023-12-18 17:25:14');
INSERT INTO `sys_oper_log` VALUES (98, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/62', '127.0.0.1', '内网IP', '{ids=62}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"L4J9ATPT2mEjRbSSuWn8f\"}', 0, NULL, '2023-12-18 17:44:46');
INSERT INTO `sys_oper_log` VALUES (99, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-18 22:31:42');
INSERT INTO `sys_oper_log` VALUES (100, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-18 22:44:39');
INSERT INTO `sys_oper_log` VALUES (101, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-18 22:47:17');
INSERT INTO `sys_oper_log` VALUES (102, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-18 22:49:38');
INSERT INTO `sys_oper_log` VALUES (103, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-18 22:52:16');
INSERT INTO `sys_oper_log` VALUES (104, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-18 22:54:51');
INSERT INTO `sys_oper_log` VALUES (105, '用户管理', 5, 'com.ruoyi.web.controller.system.SysUserController.exportByStream()', 'POST', 1, 'admin', NULL, '/system/user/exportByStream', '127.0.0.1', '内网IP', '{\"admin\":false,\"params\":{}}', 'null', 0, NULL, '2023-12-19 00:16:17');
INSERT INTO `sys_oper_log` VALUES (106, '用户管理', 5, 'com.ruoyi.web.controller.system.SysUserController.exportByStream()', 'POST', 1, 'admin', NULL, '/system/user/exportByStream', '127.0.0.1', '内网IP', '{\"admin\":false,\"params\":{}}', 'null', 0, NULL, '2023-12-19 00:16:25');
INSERT INTO `sys_oper_log` VALUES (107, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"message\",\"requestId\":\"73ce9f10-2f66-4b3e-bc4c-1dba52a6b8d2\"}', 0, NULL, '2023-12-19 15:05:30');
INSERT INTO `sys_oper_log` VALUES (108, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStream()', 'POST', 1, 'admin', NULL, '/commendation/info/exportByStream', '127.0.0.1', '内网IP', '{\"params\":{}}', 'null', 0, NULL, '2023-12-19 17:22:37');
INSERT INTO `sys_oper_log` VALUES (109, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"message\",\"requestId\":\"34fd068e-c5c4-4a87-94c1-b83b81afdfa3\"}', 0, NULL, '2023-12-19 17:29:44');
INSERT INTO `sys_oper_log` VALUES (110, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"恭喜您，数据已全部导入成功！本次导入共 11 条, 耗时0秒\",\"requestId\":\"7ed3e328-68cd-4381-a305-202cb2a77426\"}', 0, NULL, '2023-12-19 17:31:57');
INSERT INTO `sys_oper_log` VALUES (111, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', 'null', 1, '', '2023-12-19 17:47:46');
INSERT INTO `sys_oper_log` VALUES (112, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', 'null', 1, '11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师', '2023-12-19 17:50:49');
INSERT INTO `sys_oper_log` VALUES (113, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', 'null', 1, '11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师', '2023-12-19 17:52:09');
INSERT INTO `sys_oper_log` VALUES (114, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"e4f68d45-38a2-4f6e-a75c-88b7a5e2e143\"}', 0, NULL, '2023-12-19 17:55:29');
INSERT INTO `sys_oper_log` VALUES (115, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', 'null', 1, 'String index out of range: 500', '2023-12-19 18:09:14');
INSERT INTO `sys_oper_log` VALUES (116, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', 'null', 1, 'String index out of range: 500', '2023-12-19 18:18:57');
INSERT INTO `sys_oper_log` VALUES (117, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"证书信息导入失败！！！<br/>11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"d7780bc6-1ac9-419e-8be6-5830691a8cdd\"}', 0, NULL, '2023-12-19 18:20:25');
INSERT INTO `sys_oper_log` VALUES (118, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"证书信息导入失败！！！<br/>11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"8f794fe2-281d-4ad8-994f-1f8db0f3da0c\"}', 0, NULL, '2023-12-19 18:20:39');
INSERT INTO `sys_oper_log` VALUES (119, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"证书信息导入失败！！！<br/>11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"e24816a5-b483-4efc-be08-b6cfee54cce4\"}', 0, NULL, '2023-12-19 18:22:51');
INSERT INTO `sys_oper_log` VALUES (120, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"证书信息导入失败！！！<br/>11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"f4649156-6941-41b3-8851-6c3affa8ece6\"}', 0, NULL, '2023-12-20 13:29:05');
INSERT INTO `sys_oper_log` VALUES (121, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"证书信息导入失败！！！<br/>11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"9773fb38-4d63-446c-994a-c97723db882a\"}', 0, NULL, '2023-12-20 13:33:11');
INSERT INTO `sys_oper_log` VALUES (122, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"证书信息导入失败！！！<br/>11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"a9e071e3-1e0f-4e48-9be7-b76193a5814e\"}', 0, NULL, '2023-12-20 13:33:23');
INSERT INTO `sys_oper_log` VALUES (123, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"证书信息导入失败！！！<br/>11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"a9eb1b22-c552-4d15-88cf-13b0787a2484\"}', 0, NULL, '2023-12-20 13:35:20');
INSERT INTO `sys_oper_log` VALUES (124, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":500,\"msg\":\"证书信息导入失败！！！<br/>11条数据校验完成。很抱歉，数据校验失败！7条数据校验通过,已经有 4 条数据格式不正确，请改正。错误如下：<br/>第3条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第6条数据中 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书<br/>第8条数据中 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级<br/>第10条数据中 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师\",\"requestId\":\"a7af823d-d7cd-446e-9eb2-8bbfa64f8e8a\"}', 0, NULL, '2023-12-20 13:35:34');
INSERT INTO `sys_oper_log` VALUES (125, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/63', '127.0.0.1', '内网IP', '{ids=63}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"V5a16JxY0V1rnviGh4I-3\"}', 0, NULL, '2023-12-20 14:29:03');
INSERT INTO `sys_oper_log` VALUES (126, '用户管理', 3, 'com.ruoyi.web.controller.system.SysUserController.remove()', 'DELETE', 1, 'admin', NULL, '/system/user/3', '127.0.0.1', '内网IP', '{userIds=3}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"xKlIpo91oUkgX-Xx2HE7v\"}', 0, NULL, '2023-12-20 14:33:42');
INSERT INTO `sys_oper_log` VALUES (127, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/7', '127.0.0.1', '内网IP', '{roleIds=7}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"0MW0TH212M2blZgPZnFfS\"}', 0, NULL, '2023-12-20 14:34:42');
INSERT INTO `sys_oper_log` VALUES (128, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/6', '127.0.0.1', '内网IP', '{roleIds=6}', 'null', 1, '前期调研已分配,不能删除', '2023-12-20 14:34:46');
INSERT INTO `sys_oper_log` VALUES (129, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/5', '127.0.0.1', '内网IP', '{roleIds=5}', 'null', 1, '运维相关已分配,不能删除', '2023-12-20 14:34:55');
INSERT INTO `sys_oper_log` VALUES (130, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/6', '127.0.0.1', '内网IP', '{roleIds=6}', 'null', 1, '前期调研已分配,不能删除', '2023-12-20 14:35:33');
INSERT INTO `sys_oper_log` VALUES (131, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"deptCheckStrictly\":false,\"flag\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"roleId\":6,\"admin\":false,\"params\":{},\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"YotzIRH1cMcQlHBpGefim\"}', 0, NULL, '2023-12-20 14:35:37');
INSERT INTO `sys_oper_log` VALUES (132, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"deptCheckStrictly\":false,\"flag\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"roleId\":6,\"admin\":false,\"params\":{},\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"TyHGTPQc1UpLgoT9tD-se\"}', 0, NULL, '2023-12-20 14:35:41');
INSERT INTO `sys_oper_log` VALUES (133, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"deptCheckStrictly\":false,\"flag\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"roleId\":6,\"admin\":false,\"params\":{},\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"_yd74YZuosMOWRnYoJ1B9\"}', 0, NULL, '2023-12-20 14:35:44');
INSERT INTO `sys_oper_log` VALUES (134, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/6', '127.0.0.1', '内网IP', '{roleIds=6}', 'null', 1, '前期调研已分配,不能删除', '2023-12-20 14:35:47');
INSERT INTO `sys_oper_log` VALUES (135, '部门管理', 3, 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/110', '127.0.0.1', '内网IP', '{deptId=110}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"N9K9p_kuIsngYmXhJ1K5t\"}', 0, NULL, '2023-12-20 14:36:00');
INSERT INTO `sys_oper_log` VALUES (136, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"oWVqI7P8DMdRVlxpyKRgR\"}', 0, NULL, '2023-12-20 14:42:46');
INSERT INTO `sys_oper_log` VALUES (137, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"ZK9bDj2gvRJUu0SLVBDW8\"}', 0, NULL, '2023-12-20 14:42:55');
INSERT INTO `sys_oper_log` VALUES (138, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"f9r22TxHk69UVx5mi2Mlw\"}', 0, NULL, '2023-12-20 15:00:15');
INSERT INTO `sys_oper_log` VALUES (139, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"1u3tKDwRBoot0iOlotEAt\"}', 0, NULL, '2023-12-20 15:02:15');
INSERT INTO `sys_oper_log` VALUES (140, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"RLH_ZgUStI8buCnUrM8pt\"}', 0, NULL, '2023-12-20 15:15:52');
INSERT INTO `sys_oper_log` VALUES (141, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"shndX69Zud0BsE-CPlXJr\"}', 0, NULL, '2023-12-20 15:22:51');
INSERT INTO `sys_oper_log` VALUES (142, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"b9YWEvNuE8iuVh2fFPpaS\"}', 0, NULL, '2023-12-20 15:45:04');
INSERT INTO `sys_oper_log` VALUES (143, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"rCt8r1ITBDpVVhb4bL0dn\"}', 0, NULL, '2023-12-20 15:45:52');
INSERT INTO `sys_oper_log` VALUES (144, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"9j3q9GTUig2l1zGQB4bnn\"}', 0, NULL, '2023-12-20 15:46:00');
INSERT INTO `sys_oper_log` VALUES (145, '角色管理', 5, 'com.ruoyi.web.controller.system.SysRoleController.responseStream()', 'POST', 1, 'admin', NULL, '/system/role/exportByStream', '127.0.0.1', '内网IP', '{\"deptCheckStrictly\":false,\"flag\":false,\"menuCheckStrictly\":false,\"admin\":false,\"params\":{}}', 'null', 0, NULL, '2023-12-20 15:58:23');
INSERT INTO `sys_oper_log` VALUES (146, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"deptCheckStrictly\":false,\"flag\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"roleId\":6,\"admin\":false,\"params\":{},\"status\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"8uVNVbETphqm39gfzarXh\"}', 0, NULL, '2023-12-20 16:02:21');
INSERT INTO `sys_oper_log` VALUES (147, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"deptCheckStrictly\":false,\"flag\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"roleId\":6,\"admin\":false,\"params\":{},\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"qOQzHBxdjPaqq-rpvOep7\"}', 0, NULL, '2023-12-20 16:02:26');
INSERT INTO `sys_oper_log` VALUES (148, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/6', '127.0.0.1', '内网IP', '{roleIds=6}', 'null', 1, '前期调研已分配,不能删除', '2023-12-20 16:02:40');
INSERT INTO `sys_oper_log` VALUES (149, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/6', '127.0.0.1', '内网IP', '{roleIds=6}', 'null', 1, '前期调研已分配,不能删除', '2023-12-20 16:02:58');
INSERT INTO `sys_oper_log` VALUES (150, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/6', '127.0.0.1', '内网IP', '{roleIds=6}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"vYoR2IMps4HFBRVLbD2FU\"}', 0, NULL, '2023-12-20 16:06:55');
INSERT INTO `sys_oper_log` VALUES (151, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/5', '127.0.0.1', '内网IP', '{roleIds=5}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"5yLon3jQeqz2uZWbrVLJD\"}', 0, NULL, '2023-12-20 16:07:02');
INSERT INTO `sys_oper_log` VALUES (152, '角色管理', 3, 'com.ruoyi.web.controller.system.SysRoleController.remove()', 'DELETE', 1, 'admin', NULL, '/system/role/4', '127.0.0.1', '内网IP', '{roleIds=4}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"ZQrCYZUj1-evTUD8Y-W4f\"}', 0, NULL, '2023-12-20 16:07:07');
INSERT INTO `sys_oper_log` VALUES (153, '岗位管理', 3, 'com.ruoyi.web.controller.system.SysPostController.remove()', 'DELETE', 1, 'admin', NULL, '/system/post/10', '127.0.0.1', '内网IP', '{postIds=10}', 'null', 1, '后期维护已分配,不能删除', '2023-12-20 16:10:20');
INSERT INTO `sys_oper_log` VALUES (154, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"恭喜您，数据已全部导入成功！本次导入共 6 条, 耗时约1秒\",\"requestId\":\"7848812e-f020-426c-8b04-f3136830fa1a\"}', 0, NULL, '2023-12-20 18:01:51');
INSERT INTO `sys_oper_log` VALUES (155, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"恭喜您，数据已全部导入成功！本次导入共 6 条, 耗时约2秒\",\"requestId\":\"853ab234-f988-49e3-a8eb-1547a778cc0a\"}', 0, NULL, '2023-12-20 18:22:55');
INSERT INTO `sys_oper_log` VALUES (156, '证书信息导入', 6, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.importData()', 'POST', 1, 'admin', NULL, '/commendation/info/importData', '127.0.0.1', '内网IP', '', '{\"code\":200,\"msg\":\"恭喜您，数据已全部导入成功！本次导入共 6 条, 耗时约1秒\",\"requestId\":\"d458d6e9-8983-46a5-9343-a4de2df5d74a\"}', 0, NULL, '2023-12-20 18:24:52');
INSERT INTO `sys_oper_log` VALUES (157, '证书信息管理', 2, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.edit()', 'PUT', 1, 'admin', NULL, '/commendation/info/edit', '127.0.0.1', '内网IP', '{\"gameType\":\"4\",\"gameAddress\":\"\",\"gameSummary\":\"\",\"certificateName\":\"试验-一等奖\",\"updateTime\":1703091278026,\"gameStudentCollege\":\"建筑与电气工程学院\",\"params\":{},\"certificateRank\":\"4\",\"gameName\":\"试验2\",\"certificateNumber\":\"CRAIC\",\"createTime\":1613814814000,\"gameMaterials\":\"\",\"gameStudentNumber\":\"201300046\",\"acquisitionTime\":1640966400000,\"gameMember\":\"曾妍\",\"id\":47,\"productionName\":\"哈哈哈\",\"guideTeacher\":\"曾妍\",\"certificateType\":\"3\",\"prizeGiving\":\"试验\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"-EK2AQvg5cJb_wLz7UXKd\"}', 0, NULL, '2023-12-21 00:54:38');
INSERT INTO `sys_oper_log` VALUES (158, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/64', '127.0.0.1', '内网IP', '{ids=64}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"j58ULqhf-F2xuc_AasrEl\"}', 0, NULL, '2023-12-26 20:23:39');
INSERT INTO `sys_oper_log` VALUES (159, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/59', '127.0.0.1', '内网IP', '{ids=59}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"Ge1hl0XBDwD_BYAB8WRWU\"}', 0, NULL, '2023-12-26 20:23:44');
INSERT INTO `sys_oper_log` VALUES (160, '证书信息管理', 5, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.exportByStreamExportIds()', 'POST', 1, 'admin', NULL, '/commendation/info/exportIds', '127.0.0.1', '内网IP', '{\"ids\":[\"46\",\"47\"]}', 'null', 0, NULL, '2023-12-26 20:24:03');
INSERT INTO `sys_oper_log` VALUES (161, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"hnML70z1OeXEDC_S0dvnw\"}', 0, NULL, '2023-12-26 20:26:14');
INSERT INTO `sys_oper_log` VALUES (162, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/user/changeStatus', '127.0.0.1', '内网IP', '{\"updateBy\":\"admin\",\"admin\":false,\"params\":{},\"userId\":2,\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"rdN_lIak1f-1YPG_68T-g\"}', 0, NULL, '2023-12-26 20:26:18');
INSERT INTO `sys_oper_log` VALUES (163, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"nickName\":\"若依\",\"roles\":[{\"deptCheckStrictly\":false,\"flag\":false,\"menuCheckStrictly\":false,\"roleId\":2,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"admin\":false,\"dataScope\":\"2\",\"params\":{},\"roleSort\":\"2\",\"status\":\"0\"}],\"sex\":\"1\",\"deptId\":105,\"phonenumber\":\"15666666666\",\"admin\":false,\"loginDate\":1701948247000,\"remark\":\"测试员 =====\",\"avatar\":\"\",\"dept\":{\"deptName\":\"测试部门\",\"leader\":\"若依\",\"children\":[],\"deptId\":105,\"orderNum\":\"3\",\"params\":{},\"parentId\":101,\"status\":\"0\"},\"delFlag\":\"0\",\"params\":{},\"userName\":\"ry\",\"userId\":2,\"createBy\":\"admin\",\"roleIds\":[2],\"createTime\":1701948247000,\"updateBy\":\"admin\",\"postIds\":[{\"$ref\":\"roleIds[0]\"}],\"loginIp\":\"127.0.0.1\",\"email\":\"zch@qq.com\",\"status\":\"0\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"fjbZ_fg7j5asHBYI5JeYW\"}', 0, NULL, '2023-12-26 20:26:29');
INSERT INTO `sys_oper_log` VALUES (164, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/46', '127.0.0.1', '内网IP', '{ids=46}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"YlnQJEBWIUkzXx4t-R1Ol\"}', 0, NULL, '2024-03-10 17:07:22');
INSERT INTO `sys_oper_log` VALUES (165, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/47,50,56,57,58,60,61,88,89,90', '127.0.0.1', '内网IP', '{ids=47,50,56,57,58,60,61,88,89,90}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"1YZBBVF7lx8IZOWFO7rPZ\"}', 0, NULL, '2024-03-10 17:07:27');
INSERT INTO `sys_oper_log` VALUES (166, '证书信息管理', 3, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/commendation/info/91,92,93', '127.0.0.1', '内网IP', '{ids=91,92,93}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"N8pV2B1_AV35hYajH6AMR\"}', 0, NULL, '2024-03-10 17:07:32');
INSERT INTO `sys_oper_log` VALUES (167, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"certificateName\":\"质量管理认证证书\",\"params\":{},\"certificateRank\":\"11\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/1_20240310171059A004.png\",\"gameName\":\"质量管理认证\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2024/03/10/1_20240310171058A003.png\",\"createTime\":1710061881870,\"acquisitionTime\":1709827200000,\"id\":94,\"productionName\":\"质量管理项目\",\"certificateType\":\"2\",\"prizeGiving\":\"质量管理局\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"_oyt0b0uN8UbdiGhX3nJR\"}', 0, NULL, '2024-03-10 17:11:21');
INSERT INTO `sys_oper_log` VALUES (168, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"3\",\"gameName\":\"会员单位评定\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2024/03/10/2_20240310171309A006.png\",\"createTime\":1710061995830,\"certificateName\":\"会员单位证书\",\"acquisitionTime\":1709913600000,\"id\":95,\"params\":{},\"productionName\":\"会员证书\",\"certificateRank\":\"3\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/2_20240310171308A005.png\",\"certificateType\":\"3\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"8TYwTfJ4WlhTXBfAGrFvP\"}', 0, NULL, '2024-03-10 17:13:15');
INSERT INTO `sys_oper_log` VALUES (169, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"4\",\"gameName\":\"个人荣誉证书\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2024/03/10/3_20240310171358A008.png\",\"createTime\":1710062046667,\"certificateName\":\"荣誉证书\",\"acquisitionTime\":1709913600000,\"id\":96,\"params\":{},\"productionName\":\"个人荣誉证书\",\"certificateRank\":\"4\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/3_20240310171338A007.png\",\"certificateType\":\"3\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"k1voT1v0s6Mp78jic24kh\"}', 0, NULL, '2024-03-10 17:14:06');
INSERT INTO `sys_oper_log` VALUES (170, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"gameName\":\"技能培训\",\"createTime\":1710062103914,\"certificateName\":\"个人技能培训证书\",\"acquisitionTime\":1710000000000,\"id\":97,\"params\":{},\"productionName\":\"个人职业技能证书\",\"certificateRank\":\"11\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/6_20240310171454A009.png\",\"certificateType\":\"2\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"HhEy9G71-ufQhV3hVFMVD\"}', 0, NULL, '2024-03-10 17:15:03');
INSERT INTO `sys_oper_log` VALUES (171, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"3\",\"gameName\":\"荣誉证书\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2024/03/10/8_20240310171559A011.png\",\"createTime\":1710062169963,\"certificateName\":\"个人荣誉证书\",\"acquisitionTime\":1709827200000,\"id\":98,\"params\":{},\"productionName\":\"运动会\",\"certificateRank\":\"11\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/8_20240310171555A010.png\",\"certificateType\":\"3\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"3YF8QF_ck7XldfctiaObH\"}', 0, NULL, '2024-03-10 17:16:09');
INSERT INTO `sys_oper_log` VALUES (172, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"1\",\"gameName\":\"广西职业培训\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2024/03/10/10_20240310171648A013.png\",\"createTime\":1710062224037,\"certificateName\":\"广西职业技术证书\",\"acquisitionTime\":1709827200000,\"id\":99,\"params\":{},\"productionName\":\"职业技能培训\",\"certificateRank\":\"0\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/10_20240310171642A012.png\",\"certificateType\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"FQK7iiGuZrZ0-fKOpF5p8\"}', 0, NULL, '2024-03-10 17:17:04');
INSERT INTO `sys_oper_log` VALUES (173, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"3\",\"gameName\":\"银河比赛\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2024/03/10/12_20240310171854A015.png\",\"createTime\":1710062345061,\"certificateName\":\"银河证书\",\"id\":100,\"params\":{},\"productionName\":\"银河比赛\",\"certificateRank\":\"12\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/11_20240310171810A014.png\",\"certificateType\":\"4\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"9BXrXbyJg6nC-E9Us4kCO\"}', 0, NULL, '2024-03-10 17:19:05');
INSERT INTO `sys_oper_log` VALUES (174, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"gameName\":\"入围奖\",\"createTime\":1710062382394,\"certificateName\":\"入围奖\",\"acquisitionTime\":1709827200000,\"id\":101,\"params\":{},\"productionName\":\"入围奖\",\"certificateRank\":\"5\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/7_20240310171938A016.png\",\"certificateType\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"Ly0ucB0iIyvlfIHjwYfMa\"}', 0, NULL, '2024-03-10 17:19:42');
INSERT INTO `sys_oper_log` VALUES (175, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"4\",\"gameName\":\"优秀指导老师\",\"certificateReverse\":\"http://localhost:8300/profile/upload/2024/03/10/5_20240310172016A018.png\",\"createTime\":1710062419420,\"certificateName\":\"优秀指导老师\",\"acquisitionTime\":1709740800000,\"id\":102,\"params\":{},\"productionName\":\"优秀指导老师\",\"certificateRank\":\"6\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/5_20240310172013A017.png\",\"certificateType\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"YoFMhMkwwTxikNRNcxA8a\"}', 0, NULL, '2024-03-10 17:20:19');
INSERT INTO `sys_oper_log` VALUES (176, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"4\",\"gameName\":\"铜奖\",\"createTime\":1710062452206,\"certificateName\":\"铜奖\",\"acquisitionTime\":1709827200000,\"id\":103,\"params\":{},\"productionName\":\"铜奖\",\"certificateRank\":\"13\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/3_20240310172049A019.png\",\"certificateType\":\"1\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"YOdcVXGJM2xVupPKv3E_x\"}', 0, NULL, '2024-03-10 17:20:52');
INSERT INTO `sys_oper_log` VALUES (177, '证书信息管理', 1, 'com.ruoyi.web.controller.certificate.ZnlCertificateInfoController.add()', 'POST', 1, 'admin', NULL, '/commendation/info/add', '127.0.0.1', '内网IP', '{\"gameType\":\"2\",\"gameName\":\"一等奖\",\"createTime\":1710062481739,\"certificateName\":\"一等奖\",\"acquisitionTime\":1710000000000,\"id\":104,\"params\":{},\"productionName\":\"一等奖\",\"certificateRank\":\"1\",\"certificateFront\":\"http://localhost:8300/profile/upload/2024/03/10/12_20240310172117A020.png\",\"certificateType\":\"2\"}', '{\"code\":200,\"msg\":\"操作成功\",\"requestId\":\"-hrvSGl0FaqXLTPERbZ9A\"}', 0, NULL, '2024-03-10 17:21:21');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 1, 1, '0', '0', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '普通角色');
INSERT INTO `sys_role` VALUES (3, '测试相关', 'test', 3, '2', 1, 1, '0', '0', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '测试角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100, NULL, NULL);
INSERT INTO `sys_role_dept` VALUES (2, 101, NULL, NULL);
INSERT INTO `sys_role_dept` VALUES (2, 104, NULL, NULL);
INSERT INTO `sys_role_dept` VALUES (2, 105, NULL, NULL);
INSERT INTO `sys_role_dept` VALUES (2, 107, NULL, NULL);
INSERT INTO `sys_role_dept` VALUES (3, 100, NULL, NULL);
INSERT INTO `sys_role_dept` VALUES (3, 101, NULL, NULL);
INSERT INTO `sys_role_dept` VALUES (3, 105, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 100, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 101, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 102, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 103, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 104, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 105, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 106, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 107, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 108, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 500, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 501, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1001, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1002, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1008, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1013, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1017, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1018, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1021, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1022, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1023, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1024, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1025, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1026, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1027, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1028, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1029, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1030, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1031, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1032, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1033, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1034, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1035, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1036, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1037, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1038, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1039, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1040, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1041, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1042, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1043, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1044, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (2, 1045, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 3, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 4, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 114, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 115, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 116, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 1055, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 1056, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 1057, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 1058, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 1059, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_role_menu` VALUES (3, 1060, '2023-12-07 19:24:07', '2023-12-07 19:24:07');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '若依', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', '', '2023-12-07 19:24:07', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ry', '若依', '00', 'zch@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-26 20:26:29', '测试员 =====');
INSERT INTO `sys_user` VALUES (3, 108, 'test', 'test', '00', 'dsadada@168.com', '15555555555', '0', '/profile/avatar/2022/07/16/blob_20220716042515A002.jpeg', '$2a$10$sIcNKQqPJ5dmsO9TXKfHGOgU6.thgYIhatp.BkHd2suX/wKeRFHWy', '0', '2', '', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', 'admin', '2023-12-07 19:24:07', 'test');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_user_post` VALUES (2, 2, '2023-12-26 20:26:29', '2023-12-26 20:26:29');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1, '2023-12-07 19:24:07', '2023-12-07 19:24:07');
INSERT INTO `sys_user_role` VALUES (2, 2, '2023-12-26 20:26:29', '2023-12-26 20:26:29');

-- ----------------------------
-- Table structure for znl_certificate_info
-- ----------------------------
DROP TABLE IF EXISTS `znl_certificate_info`;
CREATE TABLE `znl_certificate_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `game_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '比赛名称',
  `game_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '赛事类别',
  `certificate_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书名称',
  `certificate_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书类型',
  `certificate_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书编号',
  `prize_giving` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '颁奖单位',
  `acquisition_time` datetime(0) NULL DEFAULT NULL COMMENT '获奖时间',
  `certificate_rank` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书获奖级别',
  `game_member` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参赛成员',
  `game_student_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参赛人员学号',
  `game_student_college` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参赛人员所属学院',
  `guide_teacher` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '指导老师名字',
  `production_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '作品名称',
  `game_summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '参赛总结',
  `game_materials` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '参赛材料',
  `game_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参赛地址',
  `certificate_front` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书正面',
  `certificate_reverse` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '证书反面',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '证书信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of znl_certificate_info
-- ----------------------------
INSERT INTO `znl_certificate_info` VALUES (94, '质量管理认证', '2', '质量管理认证证书', '2', NULL, '质量管理局', '2024-03-08 00:00:00', '11', NULL, NULL, NULL, NULL, '质量管理项目', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/1_20240310171059A004.png', 'http://localhost:8300/profile/upload/2024/03/10/1_20240310171058A003.png', NULL, '2024-03-10 17:11:22', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (95, '会员单位评定', '3', '会员单位证书', '3', NULL, NULL, '2024-03-09 00:00:00', '3', NULL, NULL, NULL, NULL, '会员证书', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/2_20240310171308A005.png', 'http://localhost:8300/profile/upload/2024/03/10/2_20240310171309A006.png', NULL, '2024-03-10 17:13:16', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (96, '个人荣誉证书', '4', '荣誉证书', '3', NULL, NULL, '2024-03-09 00:00:00', '4', NULL, NULL, NULL, NULL, '个人荣誉证书', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/3_20240310171338A007.png', 'http://localhost:8300/profile/upload/2024/03/10/3_20240310171358A008.png', NULL, '2024-03-10 17:14:07', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (97, '技能培训', '1', '个人技能培训证书', '2', NULL, NULL, '2024-03-10 00:00:00', '11', NULL, NULL, NULL, NULL, '个人职业技能证书', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/6_20240310171454A009.png', NULL, NULL, '2024-03-10 17:15:04', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (98, '荣誉证书', '3', '个人荣誉证书', '3', NULL, NULL, '2024-03-08 00:00:00', '11', NULL, NULL, NULL, NULL, '运动会', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/8_20240310171555A010.png', 'http://localhost:8300/profile/upload/2024/03/10/8_20240310171559A011.png', NULL, '2024-03-10 17:16:10', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (99, '广西职业培训', '1', '广西职业技术证书', '1', NULL, NULL, '2024-03-08 00:00:00', '0', NULL, NULL, NULL, NULL, '职业技能培训', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/10_20240310171642A012.png', 'http://localhost:8300/profile/upload/2024/03/10/10_20240310171648A013.png', NULL, '2024-03-10 17:17:04', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (100, '银河比赛', '3', '银河证书', '4', NULL, NULL, NULL, '12', NULL, NULL, NULL, NULL, '银河比赛', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/11_20240310171810A014.png', 'http://localhost:8300/profile/upload/2024/03/10/12_20240310171854A015.png', NULL, '2024-03-10 17:19:05', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (101, '入围奖', '2', '入围奖', '1', NULL, NULL, '2024-03-08 00:00:00', '5', NULL, NULL, NULL, NULL, '入围奖', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/7_20240310171938A016.png', NULL, NULL, '2024-03-10 17:19:42', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (102, '优秀指导老师', '4', '优秀指导老师', '1', NULL, NULL, '2024-03-07 00:00:00', '6', NULL, NULL, NULL, NULL, '优秀指导老师', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/5_20240310172013A017.png', 'http://localhost:8300/profile/upload/2024/03/10/5_20240310172016A018.png', NULL, '2024-03-10 17:20:19', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (103, '铜奖', '4', '铜奖', '1', NULL, NULL, '2024-03-08 00:00:00', '13', NULL, NULL, NULL, NULL, '铜奖', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/3_20240310172049A019.png', NULL, NULL, '2024-03-10 17:20:52', NULL, NULL, NULL);
INSERT INTO `znl_certificate_info` VALUES (104, '一等奖', '2', '一等奖', '2', NULL, NULL, '2024-03-10 00:00:00', '1', NULL, NULL, NULL, NULL, '一等奖', NULL, NULL, NULL, 'http://localhost:8300/profile/upload/2024/03/10/12_20240310172117A020.png', NULL, NULL, '2024-03-10 17:21:22', NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
