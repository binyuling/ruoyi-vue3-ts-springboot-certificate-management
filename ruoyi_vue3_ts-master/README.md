# ruoyi_vue3_springboot_certificate_management
### 介绍
> 项目雏形是基于 "冷冷的菜哥" ruoyi_vue3_ts 的原型上改造开发出来的证书管理系统。证书管理模块采用比较流行的 element-plus + vue3 + ts 写法，后端依赖于若依中的后端版本，具体文档参见若依文档。再次感谢冷冷的菜哥，再次感谢若依！

注意：
前后端与若依原本的有所差异，启动项目之前请先使用sql脚本初始化好数据库，并且在对应的配置文件中配置好数据库与redis。
你也可以直接执行 /sql/znl_project_upgrade.sql ,即可初始化所有的数据库表语句，包含演示中的demo数据。

### 相关说明

    后端——ruoyi,前端——ruoyi_ui_ts。springboot版本升级到2.6.6；
    冷冷的菜哥仓库地址：https://gitee.com/lyforvue/ruoyi_vue3_ts
    若依官网：http://www.ruoyi.vip/
    ele-plus组件地址：https://element-plus.org/zh-CN/component/overview.html
    Apache ECharts可视化图表库：https://echarts.apache.org/zh/index.html



### 内置功能

    首页：集成了ECharts可视化图表库，显示证书分布情况
    用户管理：用户是系统操作者，该功能主要完成系统用户配置。
    部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
    岗位管理：配置系统用户所属担任职务。
    菜单管理：配置系统菜单，操作权限，按钮权限标识等。
    角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
    字典管理：对系统中经常使用的一些较为固定的数据进行维护。
    参数管理：对系统动态配置常用参数。
    通知公告：系统通知公告信息发布维护。
    操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
    登录日志：系统登录日志记录查询包含登录异常。
    在线用户：当前系统中活跃用户状态监控。
    定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
    代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
    接口文档：knife4j文档与swagger文档。
    服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
    缓存监控：对系统的缓存信息查询，命令统计等。
    数据监控：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
    证书管理：包含证书的增删改查、导入、导出，批量操作等功能

### 后端开发说明

    jdk版本：1.8.0_131
    maven版本：3.6.3
    mysql版本：5.7.28
    redis：请自行安装配置
    部署启动：请先使用SQL脚本初始化数据库再启动
    开发工具：社区版-IntelliJIDEA2023.1.3
    数据库工具：navicat premium

![输入图片说明](ruoyi/doc/image/jdk-maven%E7%89%88%E6%9C%AC.png)
![输入图片说明](ruoyi/doc/image/mysql%E7%89%88%E6%9C%AC.png)
![输入图片说明](ruoyi/doc/image/%E5%90%8E%E7%AB%AF%E5%90%AF%E5%8A%A8.png)
### 前端开发说明
    开发工具：Visual Studio Code
    npm版本：8.5.5
    node版本：v16.15.0
    安装依赖请运行：npm install
    开发环境启动请运行: npm run dev
    开发环境打包请运行: npm run build
    正式环境打包请运行：npm run build:prod

![输入图片说明](ruoyi/doc/image/npm-node%E7%89%88%E6%9C%AC.png)
![输入图片说明](ruoyi/doc/image/%E5%89%8D%E7%AB%AF%E5%90%AF%E5%8A%A8.png)
### 演示视频

尚未录制

### 相关截图


##### 首页1
![输入图片说明](ruoyi/doc/image/%E9%A6%96%E9%A1%B51.png)

##### 首页2
![输入图片说明](ruoyi/doc/image/%E9%A6%96%E9%A1%B52.png)

##### 首页图标搜索框
![输入图片说明](ruoyi/doc/image/%E9%A6%96%E9%A1%B5%E6%90%9C%E7%B4%A2%E6%A1%86.png)

##### 预览1
![输入图片说明](ruoyi/doc/image/%E9%A2%84%E6%8F%BD.png)

##### 预览证书2
![输入图片说明](ruoyi/doc/image/%E9%A2%84%E8%A7%882.png)

##### 添加证书
![输入图片说明](ruoyi/doc/image/%E6%B7%BB%E5%8A%A0%E8%AF%81%E4%B9%A6.png)

##### 证书详情
https://img2024.cnblogs.com/blog/2138456/202403/2138456-20240310180708256-1794143802.png


##### 编辑证书
![输入图片说明](ruoyi/doc/image/%E7%BC%96%E8%BE%91%E9%A1%B5%E9%9D%A2.png)

##### 导入证书
![输入图片说明](ruoyi/doc/image/%E5%AF%BC%E5%85%A5.png)

##### 删除证书
![输入图片说明](ruoyi/doc/image/%E5%88%A0%E9%99%A4.png)


参与贡献

    Fork 本仓库
    新建 Feat_xxx 分支
    提交代码
    新建 Pull Request
