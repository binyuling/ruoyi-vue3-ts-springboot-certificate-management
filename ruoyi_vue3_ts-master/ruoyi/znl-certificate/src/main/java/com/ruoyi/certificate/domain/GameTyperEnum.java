package com.ruoyi.certificate.domain;

/**
 * @author love ice
 * @create 2023-12-25 17:30
 */
public enum GameTyperEnum {
    PERSONAL_PATENT_OR_COPYRIGHT(1, "国家级"),
    PERSONAL_VOCATIONAL(2, "省部级"),
    PERSONAL_HONOR(3, "地市级"),
    PERSONAL_TRAINING(4, "校级");

    private final int code;
    private final String description;

    GameTyperEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static String getDescriptionByCode(int code) {
        for (GameTyperEnum award : GameTyperEnum.values()) {
            if (award.code == code) {
                return award.description;
            }
        }
        return "Unknown";
    }
}
