import request from '@/utils/request'


/**
 * 查询证书信息管理列表
 *
 * @param {object} query
 */
export const listInfo = async (query: object) => {
  return await request({
    url: '/commendation/info/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询证书信息管理详细
 *
 * @param {string} id
 */
export const getInfo = async (id:string) => {
  return await request({
    url: '/commendation/info/' + id,
    method: 'get'
  });
};

/**
 * 新增证书信息管理
 *
 * @param {object} data
 */
export const addInfo =async (data:object) => {
  return await request({
    url: '/commendation/info/add',
    method: 'post',
    data: data
  });
};

/**
 *  修改证书信息管理
 *
 * @param {object} data
 */
export const updateInfo =async (data: object) => {
  return await request({
    url: '/commendation/info/edit',
    method: 'put',
    data: data
  });
};

/**
 * 删除证书信息管理
 *
 * @param {string} id
 */
export const delInfo =async (id:string) => {
  return await request({
    url: '/commendation/info/' + id,
    method: 'delete'
  });
};

/**
 * 根据id导出证书信息管理
 *
 * @param {object} id
 */
export const exportInfoIds =async (id:string) => {
    console.log("exportInfoIds============"+id);
    return await request({
      url: '/commendation/info/exportIds/'+id,
      method: 'get'
    });
  };


/**
 * 导出证书信息管理
 *
 * @param {object} query
 */
export const exportInfo =async (query: object) => {
  return await request({
    url: '/commendation/info/export',
    method: 'get',
    params: query
  });
};


