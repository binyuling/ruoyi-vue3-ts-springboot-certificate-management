package com.ruoyi.certificate.service.impl;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.certificate.domain.ZnlCertificateInfo;
import com.ruoyi.certificate.mapper.ZnlCertificateInfoMapper;
import com.ruoyi.certificate.service.IZnlCertificateInfoService;
import com.ruoyi.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 证书信息管理Service业务层处理
 * 
 * @author zhangch
 * @date 2023-12-07 23:37:50
 */
@Service
public class ZnlCertificateInfoServiceImpl implements IZnlCertificateInfoService {

    private static final Logger log = LoggerFactory.getLogger(ZnlCertificateInfoServiceImpl.class);

    @Resource
    ZnlCertificateInfoMapper znlCertificateInfoMapper;

    /**
     * 查询证书信息管理
     * 
     * @param id 证书信息管理ID
     * @return 证书信息管理
     */
    @Override
    public ZnlCertificateInfo selectZnlCertificateInfoById(Long id) {
        return this.znlCertificateInfoMapper.selectZnlCertificateInfoById(id);
    }

    /**
     * 批量查询证书信息管理
     *
     * @param ids 证书信息管理ID
     * @return 证书信息管理
     */
    @Override
    public List<ZnlCertificateInfo> selectZnlCertificateInfoByIds(Long[] ids) {
        return this.znlCertificateInfoMapper.selectZnlCertificateInfoByIds(ids);
    }

    /**
     * 查询证书信息管理列表
     * 
     * @param znlCertificateInfo 证书信息管理
     * @return 证书信息管理集合
     */
    @Override
    public List<ZnlCertificateInfo> selectZnlCertificateInfoList(ZnlCertificateInfo znlCertificateInfo) {
        return this.znlCertificateInfoMapper.selectZnlCertificateInfoList(znlCertificateInfo);
    }

    /**
     * 新增证书信息管理
     * 
     * @param znlCertificateInfo 证书信息管理
     * @return 执行条数
     */
    @Override
    public int insertZnlCertificateInfo(ZnlCertificateInfo znlCertificateInfo) {
        znlCertificateInfo.setCreateTime(LocalDateTime.now());
        return this.znlCertificateInfoMapper.insertZnlCertificateInfo(znlCertificateInfo);
    }

    @Override
    @Transactional
    public String importCertificateData(List<ZnlCertificateInfo> certificateList) {
        if (StringUtils.isNull(certificateList) || certificateList.size() == 0) {
            throw new ServiceException("导入证书信息数据不能为空！");
        }
        if (certificateList.size() > 1000) {
            throw new ServiceException("单次批量导入不允许超过1000条！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        int currentNumber = 0;
        for (ZnlCertificateInfo certificateInfo : certificateList) {
            currentNumber = currentNumber + 1;
            // 逻辑校验三个参数: game_type\certificate_type\certificate_rank
            String gameType = certificateInfo.getGameType();
            if (StringUtils.isEmpty(gameType)) {
                failureNum++;
                failureMsg.append("<br/>" + "第" + currentNumber + "条数据中" + " 赛事类别 字段无法匹配、可选项为: 国家级、省部级、地市级、校级");
                log.error("第" + currentNumber + "条数据中,批量导入失败-赛事类别:字段无法匹配、可选项为: 国家级、省部级、地市级、校级");
            }

            String certificateType = certificateInfo.getCertificateType();
            if (StringUtils.isEmpty(certificateType)) {
                failureNum++;
                failureMsg.append("<br/>" + "第" + currentNumber + "条数据中" + " 证书类型 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书");
                log.error("第" + currentNumber + "条数据中" + "批量导入失败-证书类型: 字段无法匹配、可选项为:个人专利/软著证书、个人职业证书、个人荣誉证书、培训证书");
            }

            String certificateRankInit = certificateInfo.getCertificateRank();
            if (StringUtils.isEmpty(certificateRankInit)) {
                failureNum++;
                failureMsg.append("<br/>" + "第" + currentNumber + "条数据中" + " 证书获奖级别 字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师");
                log.error("第" + currentNumber + "条数据中" + "批量导入失败-获奖级别:" + certificateRankInit + "字段无法匹配、可选项为: 特等奖、金奖、一等奖、银奖、二等奖、铜奖、三等奖、优秀奖、入围奖、优秀指导老师");
            }
        }


        if (failureNum > 0) {
            failureMsg.insert(0, "证书信息导入失败！！！"+"<br/>"+certificateList.size() + "条数据校验完成。很抱歉，数据校验失败！" + (certificateList.size() - failureNum) + "条数据校验通过," + "已经有 " + failureNum + " 条数据格式不正确，请改正。错误如下：");
            throw new ServiceException(failureMsg.toString());
        }

        // 数据一条条导入:后期数据量大,在考虑做批量插入
        long start = System.currentTimeMillis();
        log.info("批量插入数据检测通过,共" + certificateList.size() + "条数据,开始批量插入数据库");
        certificateList.forEach(certificateInfo -> {
            try {
                znlCertificateInfoMapper.insertZnlCertificateInfo(certificateInfo);
            } catch (Exception e) {
                log.error("批量插入数据异常,本次插入的数据为:" + certificateInfo, e);
                failureMsg.insert(0, "很抱歉，数据校验通过,此数据" + certificateInfo + "导入失败！请确保数据没有特殊字符或其他问题");
                throw new ServiceException(failureMsg.toString());
            }
        });
        long end = System.currentTimeMillis();
        long time = (end - start) / 1000;
        log.info("数据已全部导入成功！本次导入共 " + certificateList.size() + " 条, 耗时" + time + "秒");
        if (time == 0){
            time = 1L;
        }
        successMsg.insert(0, "恭喜您，数据已全部导入成功！本次导入共 " + certificateList.size() + " 条, 耗时约" + time + "秒");
        return successMsg.toString();
    }

    /**
     * 修改证书信息管理
     * 
     * @param znlCertificateInfo 证书信息管理
     * @return 执行条数
     */
    @Override
    public int updateZnlCertificateInfo(ZnlCertificateInfo znlCertificateInfo) {
        znlCertificateInfo.setUpdateTime(LocalDateTime.now());
        return this.znlCertificateInfoMapper.updateZnlCertificateInfo(znlCertificateInfo);
    }

    /**
     * 批量删除证书信息管理
     * 
     * @param ids 需要删除的证书信息管理ID
     * @return 执行条数
     */
    @Override
    public int deleteZnlCertificateInfoByIds(Long[] ids) {
        return this.znlCertificateInfoMapper.deleteZnlCertificateInfoByIds(ids);
    }

    /**
     * 删除证书信息管理信息
     * 
     * @param id 证书信息管理ID
     * @return 执行条数
     */
    @Override
    public int deleteZnlCertificateInfoById(Long id) {
        return this.znlCertificateInfoMapper.deleteZnlCertificateInfoById(id);
    }

}
