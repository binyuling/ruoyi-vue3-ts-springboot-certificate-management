package com.ruoyi.certificate.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 证书信息管理对象 znl_certificate_info
 * 
 * @author zhangch
 * @date 2023-12-07 23:37:50
 */
@ApiModel(value = "证书信息管理对象")
public class ZnlCertificateInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    private Long id;

    /**
     * 比赛名称
     */
    @ApiModelProperty(value = "比赛名称")
    @Excel(name = "比赛名称")
    private String gameName;

    /**
     * 赛事类别
     */
    @ApiModelProperty(value = "赛事类别")
    @Excel(name = "赛事类别",readConverterExp = "1=国家级,2=省部级,3=地市级,4=校级",
            combo = {"国家级", "省部级", "地市级","校级"}
           )
    private String gameType;

    /**
     * 证书名称
     */
    @ApiModelProperty(value = "证书名称")
    @Excel(name = "证书名称")
    private String certificateName;

    /**
     * 证书类型
     */
    @ApiModelProperty(value = "证书类型")
    @Excel(name = "证书类型",
            readConverterExp = "1=个人专利/软著证书,2=个人职业证书,3=个人荣誉证书,4=个人培训证书",
            combo = {"个人专利/软著证书", "个人职业证书", "个人荣誉证书","个人培训证书"}
           )
    private String certificateType;

    /**
     * 获奖级别
     */
    @ApiModelProperty(value = "获奖级别")
    @Excel(name = "获奖级别",
            readConverterExp = "0=特等奖,11=金奖,1=一等奖,12=银奖,2=二等奖,13=铜奖,3=三等奖,4=优秀奖,5=入围奖,6=优秀指导老师",
            combo = {"特等奖", "金奖", "一等奖","银奖","二等级","铜奖","三等奖","优秀奖","入围奖","优秀指导老师"})
    private String certificateRank;


    /**
     * 证书编号
     */
    @ApiModelProperty(value = "证书编号")
    @Excel(name = "证书编号")
    private String certificateNumber;

    /**
     * 颁奖单位
     */
    @ApiModelProperty(value = "颁奖单位")
    @Excel(name = "颁奖单位")
    private String prizeGiving;

    /**
     * 获奖时间
     */
    @ApiModelProperty(value = "获奖时间")
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "获奖时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date acquisitionTime;

    private Date beginTime;
    private Date endTime;

    /**
     * 参赛成员
     */
    @ApiModelProperty(value = "参赛成员")
    @Excel(name = "参赛成员")
    private String gameMember;

    /**
     * 参赛人员学号
     */
    @ApiModelProperty(value = "参赛人员学号")
    @Excel(name = "参赛人员学号")
    private String gameStudentNumber;

    /**
     * 所属学院
     */
    @ApiModelProperty(value = "所属学院")
    @Excel(name = "所属学院")
    private String gameStudentCollege;

    /**
     * 老师名字
     */
    @ApiModelProperty(value = "老师名字")
    @Excel(name = "老师名字")
    private String guideTeacher;

    /**
     * 作品名称
     */
    @ApiModelProperty(value = "作品名称")
    @Excel(name = "作品名称")
    private String productionName;

    /**
     * 参赛总结
     */
    @ApiModelProperty(value = "参赛总结")
    @Excel(name = "参赛总结")
    private String gameSummary;

    /**
     * 参赛材料
     */
    @ApiModelProperty(value = "参赛材料")
    @Excel(name = "参赛材料")
    private String gameMaterials;

    /**
     * 参赛地址
     */
    @ApiModelProperty(value = "参赛地址")
    @Excel(name = "参赛地址")
    private String gameAddress;

    /**
     * 证书正面
     */
    @ApiModelProperty(value = "证书正面")
//    @Excel(name = "证书正面")
    private String certificateFront;

    /**
     * 证书反面
     */
    @ApiModelProperty(value = "证书反面")
//    @Excel(name = "证书反面")
    private String certificateReverse;


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId(){
        return id;
    }
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public String getGameName(){
        return gameName;
    }
    public void setGameType(String gameType) {
        this.gameType = gameType;
    }

    public String getGameType(){
        return gameType;
    }
    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public String getCertificateName(){
        return certificateName;
    }
    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getCertificateType(){
        return certificateType;
    }
    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public String getCertificateNumber(){
        return certificateNumber;
    }
    public void setPrizeGiving(String prizeGiving) {
        this.prizeGiving = prizeGiving;
    }

    public String getPrizeGiving(){
        return prizeGiving;
    }

    public void setAcquisitionTime(Date acquisitionTime) {
        this.acquisitionTime = acquisitionTime;
    }

    public Date getAcquisitionTime(){
        return acquisitionTime;
    }
    public void setCertificateRank(String certificateRank) {
        this.certificateRank = certificateRank;
    }

    public String getCertificateRank(){
        return certificateRank;
    }
    public void setGameMember(String gameMember) {
        this.gameMember = gameMember;
    }

    public String getGameMember(){
        return gameMember;
    }
    public void setGameStudentNumber(String gameStudentNumber) {
        this.gameStudentNumber = gameStudentNumber;
    }

    public String getGameStudentNumber(){
        return gameStudentNumber;
    }
    public void setGameStudentCollege(String gameStudentCollege) {
        this.gameStudentCollege = gameStudentCollege;
    }

    public String getGameStudentCollege(){
        return gameStudentCollege;
    }
    public void setGuideTeacher(String guideTeacher) {
        this.guideTeacher = guideTeacher;
    }

    public String getGuideTeacher(){
        return guideTeacher;
    }
    public void setProductionName(String productionName) {
        this.productionName = productionName;
    }

    public String getProductionName(){
        return productionName;
    }
    public void setGameSummary(String gameSummary) {
        this.gameSummary = gameSummary;
    }

    public String getGameSummary(){
        return gameSummary;
    }
    public void setGameMaterials(String gameMaterials) {
        this.gameMaterials = gameMaterials;
    }

    public String getGameMaterials(){
        return gameMaterials;
    }
    public void setGameAddress(String gameAddress) {
        this.gameAddress = gameAddress;
    }

    public String getGameAddress(){
        return gameAddress;
    }
    public void setCertificateFront(String certificateFront) {
        this.certificateFront = certificateFront;
    }

    public String getCertificateFront(){
        return certificateFront;
    }
    public void setCertificateReverse(String certificateReverse) {
        this.certificateReverse = certificateReverse;
    }

    public String getCertificateReverse(){
        return certificateReverse;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("gameName", getGameName())
            .append("gameType", getGameType())
            .append("certificateName", getCertificateName())
            .append("certificateType", getCertificateType())
            .append("certificateNumber", getCertificateNumber())
            .append("prizeGiving", getPrizeGiving())
            .append("acquisitionTime", getAcquisitionTime())
            .append("certificateRank", getCertificateRank())
            .append("gameNumber", getGameMember())
            .append("gameStudentNumber", getGameStudentNumber())
            .append("gameStudentCollege", getGameStudentCollege())
            .append("guideTeacher", getGuideTeacher())
            .append("productionName", getProductionName())
            .append("gameSummary", getGameSummary())
            .append("gameMaterials", getGameMaterials())
            .append("gameAddress", getGameAddress())
            .append("certificateFront", getCertificateFront())
            .append("certificateReverse", getCertificateReverse())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }

}
