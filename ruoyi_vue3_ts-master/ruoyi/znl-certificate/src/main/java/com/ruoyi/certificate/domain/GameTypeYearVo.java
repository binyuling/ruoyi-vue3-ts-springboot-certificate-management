package com.ruoyi.certificate.domain;

import java.util.List;

/**
 * {
 *     source: [
 *       ['product', '国家级', '省部级', '地市级','校级'],
 *       ['2015', 30, 30, 40,10],
 *       ['2016', 83.1, 73.4, 55.1,10],
 *       ['2017', 86.4, 65.2, 82.5,10],
 *       ['2018', 72.4, 53.9, 39.1,10]
 *     ]
 *   }
 * @author love ice
 * @create 2023-12-20 18:46
 */
public class GameTypeYearVo {
    private List<List<String>> source;

    public List<List<String>> getSource() {
        return source;
    }

    public void setSource(List<List<String>> source) {
        this.source = source;
    }
}
