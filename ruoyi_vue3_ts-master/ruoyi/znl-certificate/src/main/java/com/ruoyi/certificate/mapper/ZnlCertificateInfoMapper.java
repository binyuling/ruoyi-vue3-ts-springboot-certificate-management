package com.ruoyi.certificate.mapper;

import com.ruoyi.certificate.domain.ZnlCertificateInfo;

import java.util.List;

/**
 * 证书信息管理Mapper接口
 *
 * @author zhangch
 * @date 2023-12-07 23:37:50
 */
public interface ZnlCertificateInfoMapper {

    /**
     * 查询证书信息管理
     *
     * @param id 证书信息管理ID
     * @return 证书信息管理
     */
     ZnlCertificateInfo selectZnlCertificateInfoById(Long id);

    List<ZnlCertificateInfo> selectZnlCertificateInfoByIds(Long[] ids);

    /**
     * 查询证书信息管理列表
     *
     * @param znlCertificateInfo 证书信息管理
     * @return 证书信息管理集合
     */
     List<ZnlCertificateInfo> selectZnlCertificateInfoList(ZnlCertificateInfo znlCertificateInfo);

    /**
     * 新增证书信息管理
     *
     * @param znlCertificateInfo 证书信息管理
     * @return 结果
     */
     int insertZnlCertificateInfo(ZnlCertificateInfo znlCertificateInfo);

    /**
     * 修改证书信息管理
     *
     * @param znlCertificateInfo 证书信息管理
     * @return 结果
     */
     int updateZnlCertificateInfo(ZnlCertificateInfo znlCertificateInfo);

    /**
     * 删除证书信息管理
     *
     * @param id 证书信息管理ID
     * @return 结果
     */
     int deleteZnlCertificateInfoById(Long id);

    /**
     * 批量删除证书信息管理
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
     int deleteZnlCertificateInfoByIds(Long[] ids);
}
