package com.ruoyi.certificate.domain;

/**
 * @author love ice
 * @create 2023-12-25 17:30
 */
public enum CertificateTyperEnum {
    PERSONAL_PATENT_OR_COPYRIGHT(1, "个人专利/软著证书"),
    PERSONAL_VOCATIONAL(2, "个人职业证书"),
    PERSONAL_HONOR(3, "个人荣誉证书"),
    PERSONAL_TRAINING(4, "个人培训证书");

    private final int code;
    private final String description;

    CertificateTyperEnum(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public static String getDescriptionByCode(int code) {
        for (CertificateTyperEnum award : CertificateTyperEnum.values()) {
            if (award.code == code) {
                return award.description;
            }
        }
        return "Unknown";
    }
}
