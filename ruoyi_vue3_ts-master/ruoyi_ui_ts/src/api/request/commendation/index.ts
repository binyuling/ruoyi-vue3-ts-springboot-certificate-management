import { ref, getCurrentInstance } from "vue";
export default ()=>{
    const { proxy } = getCurrentInstance() as any;
    	// 状态数据字典
	const certificateRankOptions = ref<any>();
	const certificateTypeOptions = ref<any>();
	const gameTypeOptions = ref<any>();
    const { znl_game_type } = proxy.useDict("znl_game_type");
    const { znl_certificate_type } = proxy.useDict("znl_certificate_type");
    const { znl_certificate_rank } = proxy.useDict("znl_certificate_rank");
    

    proxy.getDicts("znl_certificate_rank").then((response: any) => {
		certificateRankOptions.value = response.data;
	});

    proxy.getDicts("znl_certificate_type").then((response: any) => {
		certificateTypeOptions.value = response.data;
	});

    proxy.getDicts("znl_game_type").then((response: any) => {
		gameTypeOptions.value = response.data;
	});


    return {
        certificateRankOptions,
        certificateTypeOptions,
        gameTypeOptions,
        znl_game_type,
        znl_certificate_type,
        znl_certificate_rank
    }
}