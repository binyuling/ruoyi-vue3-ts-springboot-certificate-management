package com.ruoyi.certificate.domain;

import java.io.Serializable;
import java.util.List;

/**
 *
 * data: [
 *         { value: 1048, name: '特等奖' },
 *         { value: 735, name: '金奖' },
 *         { value: 580, name: '一等奖' },
 *         { value: 484, name: '银奖' },
 *         { value: 300, name: '二等奖' },
 *         { value: 300, name: '铜奖' },
 *         { value: 300, name: '三等奖' },
 *         { value: 300, name: '优秀奖' },
 *         { value: 300, name: '入围奖' },
 *         { value: 300, name: '优秀指导老师' }
 *       ]
 *
 * @author love ice
 * @create 2023-12-22 16:24
 */
public class CertificateRankVo implements Serializable {
    private Integer value;

    private String name;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
