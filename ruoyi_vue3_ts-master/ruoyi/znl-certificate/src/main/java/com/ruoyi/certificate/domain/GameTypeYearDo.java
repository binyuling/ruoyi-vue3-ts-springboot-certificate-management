package com.ruoyi.certificate.domain;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 按照年份，统计每年中的赛事类别比赛
 * @author love ice
 * @create 2023-12-20 18:28
 */
public class GameTypeYearDo extends BaseEntity {


    // 年份
    private String year;

    // 国家级别
    private String nationalCount;

    // 省级别
    private String provinceCount;
    // 市级别
    private String cityCount;
    // 校级别
    private String schoolCount;

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNationalCount() {
        return nationalCount;
    }

    public void setNationalCount(String nationalCount) {
        this.nationalCount = nationalCount;
    }

    public String getProvinceCount() {
        return provinceCount;
    }

    public void setProvinceCount(String provinceCount) {
        this.provinceCount = provinceCount;
    }

    public String getCityCount() {
        return cityCount;
    }

    public void setCityCount(String cityCount) {
        this.cityCount = cityCount;
    }

    public String getSchoolCount() {
        return schoolCount;
    }

    public void setSchoolCount(String schoolCount) {
        this.schoolCount = schoolCount;
    }
}
