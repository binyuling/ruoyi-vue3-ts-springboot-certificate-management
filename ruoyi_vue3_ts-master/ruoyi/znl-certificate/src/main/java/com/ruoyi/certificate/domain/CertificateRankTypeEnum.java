package com.ruoyi.certificate.domain;

import java.util.Objects;

/**
 * @author love ice
 * @create 2023-12-25 17:39
 */
public enum CertificateRankTypeEnum {
    CERTIFICATE_RANK("1","获奖级别"),
    CERTIFICATE_TYPE("2","证书类型"),
    GAME_TYPE("3","赛事类别");



    private final String code;
    private final String description;
    CertificateRankTypeEnum(String code,String description){
        this.code = code;
        this.description = description;
    }
    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
    public static String getDescriptionByCode(String code) {
        for (CertificateRankTypeEnum award : CertificateRankTypeEnum.values()) {
            if (Objects.equals(award.code, code)) {
                return award.description;
            }
        }
        return "Unknown";
    }
    public static CertificateRankTypeEnum getByCode(String code) {
        for (CertificateRankTypeEnum type : CertificateRankTypeEnum.values()) {
            if (Objects.equals(type.code, code)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid certificate rank type code: " + code);
    }
}
