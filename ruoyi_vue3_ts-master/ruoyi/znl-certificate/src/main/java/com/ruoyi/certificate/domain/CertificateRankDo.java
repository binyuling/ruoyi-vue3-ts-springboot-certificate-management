package com.ruoyi.certificate.domain;

/**
 * @author love ice
 * @create 2023-12-24 21:56
 */
public class CertificateRankDo {
   private String dateKey;
   private Integer count;

   public String getDateKey() {
      return dateKey;
   }

   public void setDateKey(String dateKey) {
      this.dateKey = dateKey;
   }

   public Integer getCount() {
      return count;
   }

   public void setCount(Integer count) {
      this.count = count;
   }
}
